<?php

namespace App\Http\Controllers;

use App\form;
use App\Location;
use Illuminate\Http\Request;
use anlutro\LaravelSettings\Facade as Setting;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'View Generator Url', $user->location_id);
        if($isAllowed){
            return view('form.index');
        }else{
            $locations=Location::all();
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }
    public function ajaxForms(Request $request){

        $data=Form::where('id','>',0)->orderBy('id','DESC');
        return DataTables::of($data)
            ->addColumn('url', function ($data) {
                if($data->token) {
                    return "<a href='" . route('publicForm', $data->token) . "'  class='btn btn-primary cpy' >Link</a><!-- Trigger -->
<button class='btn btn-success' data-clipboard-text='". route('publicForm', $data->token)."'>
    Copy to clipboard
</button>";
                }else{
                    return "<a href='" . route('form.show', $data->id) . "'  class='btn btn-primary'>Link</a><button class='btn btn-success' data-clipboard-text='". route('form.show', $data->id)."'>
    Copy to clipboard
</button>";

                }
            })
            ->rawColumns(["url"])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Create Generator Url', $user->location_id);
        if($isAllowed){
            return view('form.create');
        }else{
            $locations=Location::all();
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form= new form();
        $form->name=$request->formname;
        $form->amount=$request->amount;
        $form->token=uniqid();
        $form->save();
        return redirect()->back()->with(['message'=>'Form Successfully Created.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(form $form)
    {
        $user=Auth::user();
        $locations=Location::all();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'View Generator Url', $user->location_id);
        if($isAllowed){
            $form=Form::where('id',$form->id)->first();
            return view('form.show',compact('form'));
        }else{
            $locations=Location::all();
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }

    }
    public function showAmount($token)
    {
         $form= Form::where('token',$token)->first();
        return view('form.showpayment',compact('form'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(form $form)
    {
        //
    }
}
