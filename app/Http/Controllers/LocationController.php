<?php

namespace App\Http\Controllers;

use App\Location;
use App\Policy;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'View Location', $user->location_id);
        if($isAllowed){

//        $slelcted_loaction = session()->get('selected_location_id');
        $current_location = "Admin";
//        if ($slelcted_loaction != null) {
//            $loc = Location::where('id', $slelcted_loaction)->first();
//            $current_location = $loc->name;


            $locations = Location::all();
            return view('location.index', compact('locations', 'current_location'));

        }else{
            return redirect(route('dashboard'))->with(['Message','You Dont have Permission to access']);
        }
    }

        public function ajaxdata(Request $request){
            $data = Location::where('id', '>', 0);
            return DataTables::of($data)
                ->addColumn('function', function (Location $location) {
                    return "<a href='". route('location.edit', $location->id)."' id='edit'>
                        <button class='btn btn-primary' title='Edit' data-toggle='modal' data-target=''><i class='fa fa-edit'></i></button></a>" . view('location.delete', compact('location'))->render();
                })
                ->rawColumns(['function'])
                ->make(true);
        }
    public function setLocation(Request $request)
    {
        $request->session()->put('selected_location_id', $request->location);

        return redirect('location');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Create Location', $user->location_id);
        if($isAllowed){
            return view('location.create');
        }else{
            return redirect(route('dashboard'))->with(['Message','You Dont have Permission to access']);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'name' => 'required|',
            'address' => 'required',
        ]);
        $location = new  Location();
        $location->name = $request->name;
        $location->address = $request->address;
        $location->save();
        $policies = Policy::all();
        $location->policies()->sync($policies);
        return redirect()->back()->with(['message'=>'Location Updated Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Edit Location', $user->location_id);
        if($isAllowed){
            return view ('location.edit',compact('location'));
        }else{
            return redirect(route('dashboard'))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Location $location)
    {
        $location->name = $request->name;
        $location->address = $request->address;
        $location->save();
        return redirect()->back()->with(['message'=>'Location Updated Successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Delete Location', $user->location_id);
        if($isAllowed){
            $location->delete();
            return redirect()->back()->with(['message'=>'Location Deleted Successfully.']);
        }else{
            return redirect(route('dashboard'))->with(['Message','You Dont have Permission to access']);
        }
    }
//    public function delete(Request $request, Location $location){
//        $user=Auth::user();
//        $user->load('roles');
//        $isAllowed = MyCheck::check($user, 'formview', $location);
////        return $isAllowed;
//        if($isAllowed){
//            return 'yeah fuck it';
//        }else{
//            return 'you cant delete it motherfucker';
//        }
//    }
}
