<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class MyCheck extends Controller
{


    public static function check($user, $permissionName, $location)
    {
        if($user->roles[0]->id == 1){
            return true;
        }
        return $user->locationPolicies()
            ->whereHas('policy', function ($q) use ($permissionName, $location) {
                return $q->where('name', $permissionName);
            })
            ->where('location_id', $location)
            ->first();
    }
}
