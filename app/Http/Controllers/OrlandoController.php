<?php

namespace App\Http\Controllers;

use App\Location;
use App\Log;
use App\Orlando;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use SplFileObject;
use Yajra\DataTables\DataTables;

//use Excel;

class OrlandoController extends Controller
{

    public function import(Request $request)
    {
        //add your system here so you can check role and permission for user to upload csvs the
        $user = Auth::user();
        $locations = Location::all();
        $isAllowed = MyCheck::check($user, 'Create Home Url', $user->location_id);
        if ($isAllowed) {

            $orlando = new Orlando();
            if ($request->hasFile('file')) {
                //add file to server get file name and date and extension
                $fileNameWithExt = $request->file('file')->getClientOriginalName();
                // Get file mime type
                $mimeType = $request->file('file')->getClientMimeType();
                // Get just name of image
                $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                // Get extention of image
                $extension = $request->file('file')->getClientOriginalExtension();
                // Filename to store the image
//            $time = Carbon::now();
//            $time->toDateString();
                $number = uniqid();
                $fileNameToStore = $fileName . '-' . $number . '.' . $extension;
                // Upload image

                Storage::disk('files')->put($fileNameToStore, File::get($request->file('file')));

                $tmpName = $_FILES['file']['tmp_name'];
                $csvAsArray = new SplFileObject($tmpName);
                $csvAsArray->setFlags(SplFileObject::READ_CSV);
                $re = array();
                $reports = '';
                $index = 1;
                $result = [];
                $start = 0;
                $d1 = date('m-d-Y h:i:s a', time());

                $d = str_replace(" ", "_", $d1);
                $d = str_replace(":", "_", $d1);
                $file_content = '';
                Storage::disk('details')->put('importreport_for_' . $d . '.txt', $file_content);

                $count = 0;
                $sampledata = [];
                foreach ($csvAsArray as $csvLine) {
                    if ($this->testingBefore($index, $csvLine)) {
                        if ($csvLine[7] == "Jobber Payments") {
                            $recieved = 'Y';
                        } elseif ($csvLine[7] == "Credit Card") {
                            $recieved = 'Y';

                        } else {
                            $recieved = 'N';
                        }
                        $orlando = Orlando::create(
                            [
                                'client_name' => str_replace("'", " ", $csvLine[0]),
                                'date' => $csvLine[1],
                                'type' => $csvLine[2],
                                'total' => (double)str_replace(",", "", $csvLine[3]),
                                'note' => preg_replace('/[^0-9a-zA-Z ]/', '', $csvLine[4]),
                                'check_' => $csvLine[5],
                                'invoice' => $csvLine[6],
                                'method' => $csvLine[7],
                                'transaction_id' => $csvLine[8],
                                'employee' => NULL,
                                'received' => $recieved,
                                'notes' => count($csvLine) > 10 ? $csvLine[11] : "",
                                'location_id' => Auth::user()->location_id
                            ]);
                        $note = count($csvLine) > 10 ? $csvLine[11] : "";
                        $reports .= $index . " " . $csvLine[0] . " " . $csvLine[1] . " " . $csvLine[2] . " " . (double)str_replace(",", "", $csvLine[3]) . " " . $note . " " . $csvLine[5] . " " . $csvLine[6] . " " . $csvLine[7] . "" . PHP_EOL;
                        $count++;
                    } else {
                        echo "false";
                    }

                    $index++;
                }
                $this->importReport($d, $reports);

            }

            if ($count < 1) {
                Storage::disk('details')->put('importreport_for_' . $d . '.txt', "No data was imported");
            }
            Storage::disk('details')->put('lastimport.txt', $d1);
            return redirect()->back()->with('message', 'Data imported Successfuly!');

        } else {
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);


        }
//
    }

    function testingBefore($index, $csvLine)
    {
        return $index > 11
            && count($csvLine) > 6
            && strtoupper($csvLine[2]) == "PAYMENT"
            && (strtoupper($csvLine[7]) == "CHECK" OR strtoupper($csvLine[7]) == "CASH" OR strtoupper($csvLine[7]) == "OTHER" OR strtoupper($csvLine[7]) == "CREDIT CARD" OR strtoupper($csvLine[7]) == "JOBBER PAYMENTS" OR strtoupper($csvLine[7]) == "REFUND")
            && $csvLine[0] != "Report totals:"
            && !is_null($csvLine[6])
            && empty(Orlando::where('invoice', $csvLine[6])->where('method', $csvLine[7])->first());
    }

    function importReport($d, $reports)
    {
        $myfile = Storage::disk('details')->put('importreport_for_' . $d . '.txt', "$reports") or die("Unable to open file!");
        $log = new Log();
        $log->file_name = 'importreport_for_' . $d . '.txt';
        $log->date = Carbon::now();
        $log->created_at = Carbon::now();
        $log->updated_at = Carbon::now();
        $log->save();

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //add your system here so you can check role and permission for user to upload csvs the
        $locations = Location::all();
        //add your system here so you can check role and permission for user to upload csvs the
        $user = Auth::user();
        $isAllowed = MyCheck::check($user, 'View Home Url', $user->location_id);
        if ($isAllowed) {
            return view('home', compact('locations'));
        } else {
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    public function paymentdata(Request $request)
    {
//        return $request->location;
        //add your system here so you can check role and permission for user to upload csvs the
        $log = Log::orderBy('id', 'DESC')->limit(1)->first();


        $sum = Orlando::groupBy('method')
            ->selectRaw('sum(total) as sum, method')
            ->when($request->methodz, function ($q) use ($request) {
                return $q->where('method', $request->methodz);
            })
            ->when($request->payments, function ($q) use ($request) {
                return $q->where('received', $request->payments);
            })
            ->when($request->datepick, function ($q) use ($request) {
                $dates = explode(' - ', $request->datepick);
                $cDate = [$dates[0], $dates[1]];
                return $q->whereBetween('orlandos.date', $cDate);
            })
            ->pluck('sum', 'method');


        $data = Orlando::
        when(Auth::user()->location_id, function ($q) {
            return $q->where('location_id', Auth::user()->location_id);
        })
            ->when($request->location, function ($q) use ($request) {
                return $q->where('location_id', $request->location);
            })
            ->when($request->methodz, function ($q) use ($request) {
                return $q->where('method', $request->methodz);
            })
            ->when($request->payments, function ($q) use ($request) {
                return $q->where('received', $request->payments);
            })
            ->when($request->datepick, function ($q) use ($request) {
                $dates = explode(' - ', $request->datepick);
                $cDate = [$dates[0], $dates[1]];
                return $q->whereBetween('orlandos.date', $cDate);
            });

        return DataTables::of($data)
            ->with('sum', str_replace('-', '', $sum))
            ->with('logs', "<a style='color:#fff!important;' href='public/uploads/reports/$log->file_name' traget='_blank'>$log->date</a>")
            ->addColumn('id', function (Orlando $orlando) {
                return '<a href="#" id="edit">
                        <button class="btn btn-primary" data-id="' . $orlando->id . '" title="Edit" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></button></a> ' . $orlando->id;
            })
            ->rawColumns(['id'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Orlando $orlando
     * @return Response
     */
    public function show(Orlando $orlando)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Orlando $orlando
     * @return Response
     */
    public function edit(Request $request)
    {
        //add your system check here
        $locations = Location::all();
        $user = Auth::user();
        $isAllowed = MyCheck::check($user, 'Edit Orlando Url', $user->location_id);
        if ($isAllowed) {
            $data = Orlando::where('id', $request->id)->where('location_id', $user->location_id)->first();
            return response()->json($data);
        } else {
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Orlando $orlando
     * @return Response
     */
    public function update(Request $request, Orlando $orlando)
    {
        $user = Auth::user();
        return Orlando::where('id', $request->id)->where('location_id',$user->location_id)
            ->update([
                'client_name' => $request->client_name,
                'date' => $request->date,
                'type' => $request->type,
                'total' => $request->total,
                'note' => $request->note,
                'check_' => $request->check,
                'invoice' => $request->invoice,
                'method' => $request->methods,
                'transaction_id' => $request->transaction,
                'employee' => $request->employee,
                'received' => $request->received,
                'notes' => $request->notes,
                'location_id' => $user->location_id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Orlando $orlando
     * @return Response
     */
    public function destroy(Orlando $orlando)
    {
        //
    }

    public function dashboard()
    {
        $locations = Location::all();
        return view('dashboard', compact('locations'));
    }
}
