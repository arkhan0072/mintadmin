<?php

namespace App\Http\Controllers;

use App\Location;
use App\Payment;
use Illuminate\Http\Request;
use anlutro\LaravelSettings\Facade as Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class PaymentController extends Controller
{

    public function payment(Request $request)
    {

        $customer='';
        \Stripe\Stripe::setApiKey(Setting::get('stripe_sk'));
            $token = $request->stripeToken;
            if($request->customer_id){
                $s=\Stripe\Charge::create([
                    'amount' => $request->amount * 100,
                    'currency' => 'usd',
                    'customer' => $request->customer_id,
                ]);
                $real_token = $request->token;
                $stripeToken = $request->stripeToken;
                $retrive = \Stripe\Charge::retrieve($s['id']);
                $payment = new Payment;
                $payment->stripe_token = $request->customer_id;
                if($s['status']=='succeeded'){
                    $payment->verified = 1;
                }else{
                    $payment->verified = 0;

                }
            }
            else{
                $customer = \Stripe\Customer::create([
                    'source' => $token,
                    'email' => $request->email,
                ]);
                $s=\Stripe\Charge::create([
                    'amount' => $request->amount * 100,
                    'currency' => 'usd',
                    'customer' => $customer->id,
                ]);
                $real_token = $request->token;
                $stripeToken = $request->stripeToken;
                $retrive = \Stripe\Charge::retrieve($s['id']);
                $payment = new Payment;
                $payment->stripe_token = $request->stripeToken;
                if($s['status']=='succeeded'){
                    $payment->verified = 1;
                }else{
                    $payment->verified = 0;

                }
            }

        $result=Payment::where('name',$request->form_email)->first();
       if(!$result) {
           $name = $s['billing_details']->name;
           $payment->amount = $s['amount'] / 100;
           $payment->email = $s['billing_details']->email;
           $payment->form_email = $request->form_email;
           $payment->name = $name;
           $payment->card_last_4 = $s['payment_method_details']->card['last4'];
           if ($customer) {
               $payment->customer_id = $customer->id;
           } else {
               $payment->customer_id = $request->customer_id;
           }
           $payment->save();
           return redirect()->back()->with('message', 'Payment Made Successfully.');

       }else{
           return redirect()->back()->with(['message','Payment Cant be made again you already paid That.']);
       }

    }

    public function index()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'View Payments', $user->location_id);
        if($isAllowed){
            return view('payment.index');
        }else{
            $locations=Location::all();
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);        }


    }
    public function makesecondpayment(Request $request){
        $data =Payment::where('id',$request->id)->first();
        return response()->json($data);

    }
    public function paymentLogs(Request $request){

        $a='';
        $data=Payment::where('id','>',0);

        return DataTables::of($data)
            ->addColumn('function', function (Payment $payment) use ($a) {
                if ($payment->verified) {
                    $a="<i class='fa fa-check' style='color:limegreen;'></i>";
                    $s='Verified';
                }else{
                    $a="<i class='fa fa-times' style='color:red;'></i>";
                    $s='Not-Verified';
                }
                    return   "<a href='#' id='edit'>
                        <button class='btn btn-secondary' title='$s' data-toggle='modal' data-target=''>". $a . "</button></a><button data-id=$payment->id' type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal'><i class='fa fa-credit-card'></i></button>";

                })
            ->rawColumns(['function'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
