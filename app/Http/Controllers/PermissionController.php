<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Location;
use Yajra\DataTables\DataTables;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations=Location::all();
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'View Permission Url', $user->location_id);
        if($isAllowed) {
            return view('permission.index');
        }else{
             return redirect(route('dashboard',compact('locations'))->with(['Message','You Dont have Permission to access']));
        }
    }
    public function ajaxPermission(Request $request)
    {
        $data = Permission::where('id', '>', 0);
        return DataTables::of($data)
            ->addColumn('function', function (Permission $permission) {
                return "<a href='".route( 'permission.edit',$permission->id) . "' id='edit'>
                        <button class='btn btn-primary' title='Edit' data-toggle='modal' data-target=''><i class='fa fa-edit'></i></button></a>" . view('permission.delete', compact('permission'))->render();
            })
            ->rawColumns(['function'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        $locations=Location::all();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Create Permission Url', $user->location_id);
        if($isAllowed) {
            return view('permission.create');
        }else{

            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permission = new Permission();
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();
        return redirect()->back()->with(['message'=>'Permission Created Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        $locations=Location::all();
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Edit Permission Url', $user->location_id);
        if($isAllowed) {
        return view('permission.edit',compact('permission'));
            }else{
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();
        return redirect()->back()->with(['message'=>'Permission Updated Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Delete Permission Url', $user->location_id);
        if($isAllowed) {
            $permission->delete();
            return redirect()->back()->with(['message' => 'Permission Deleted Successfully.']);
        }else{
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }

    }
}
