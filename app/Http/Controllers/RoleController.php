<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'View Role Url', $user->location_id);
        if($isAllowed) {
            return view('role.index');
        }else{
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }
    public function ajaxRole(Request $request)
    {
        $data = Role::where('id', '>', 0);
        return DataTables::of($data)
            ->addColumn('function', function (Role $role) {
                return "<a href=" . 'role/' . $role->id . '/edit' . " id='edit'>
                        <button class='btn btn-primary' title='Edit' data-toggle='modal' data-target=''><i class='fa fa-edit'></i></button></a>" . view('role.delete', compact('role'))->render();
            })
            ->rawColumns(['function'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Create Role Url', $user->location_id);
        if($isAllowed) {
            return view('role.create');
        }else{
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role();
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();
        return redirect()->back()->with(['message'=>'Role Successfully Created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Create Role Url', $user->location_id);
        if($isAllowed) {
            return view('role.edit', compact('role'));
        }else{
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();
        return redirect()->back()->with(['message'=>'Role Successfully Updated.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Delete Role Url', $user->location_id);
        if($isAllowed) {
            $role->delete();
            return redirect()->back()->with(['message' => 'Role Successfully Deleted.']);
        }else{
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }
}
