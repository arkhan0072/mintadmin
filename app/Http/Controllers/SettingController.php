<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use anlutro\LaravelSettings\Facade as Setting;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{

    public  function  show(){
       $user=Auth::user();
        if($user->roles->pluck( 'name' )->contains( 'Admin' )) {
            return view('user.settings');
        }else{
            return redirect('home');
        }
    }
    public function update(Request $request){

//        Setting::set('PAYPAL_CLIENT_ID',$request->PAYPAL_CLIENT_ID);
//        Setting::set('PAYPAL_SECRET',$request->PAYPAL_SECRET);
//        Setting::set('PAYPAL_IS_LIVE',$request->PAYPAL_IS_LIVE);

            Setting::set('stripe_pk',$request->stripe_pk);
            Setting::set('stripe_sk',$request->stripe_sk);


        Setting::save();
        return view('user.settings');

    }

}
