<?php

namespace App\Http\Controllers;

use App\Location;
use App\Permission;
use App\Policy;
use App\Role;
use App\User;
use App\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'View User Url', $user->location_id);
        if($isAllowed) {
            if($user->location_id==0){
                $data = User::all();

            }else{
            $data = User::where('location_id',$user->location_id)->get();
            }
            return view('user.index', compact('data','isAllowed'));
        }else{
            return redirect(route('dashboard',compact('locations')))->with(['Message','You Dont have Permission to access']);
        }
    }

    public function ajaxUsers(Request $request)
    {
        $data = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->join('locations', 'locations.id', '=', 'users.location_id')
            ->select('users.*','users.id','locations.name as Location Name', 'users.name as username', 'role_user.*', 'roles.*');
//        return $data;
        return DataTables::of($data)
            ->addColumn('function', function ($data) {
                return "<a href=".'user/'.$data->user_id.'/edit'." id='edit'>
                        <button class='btn btn-primary' title='Edit' data-toggle='modal' data-target=''><i class='fa fa-edit'></i></button></a>". view('user.delete', compact('data'))->render() ;
            })
            ->rawColumns(['function'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Create User Url', $user->location_id);
        if($isAllowed) {

        $locations = Location::with('policies')->get();
        $models = ['App\User','App\Orlando','App\Permission','App\Role','App\Location'];
//        return $models;
        $roles = Role::all();
//        return $locations;
//        $polices = Policy::all();
        return view('user.create', compact('roles', 'locations','models'));
        }else{
            return redirect(route('dashboard'))->with(['Message','You Dont have Permission to access']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        $request->validate([
//            'username' => 'required',
//            'email' => 'required',
//            'password' => 'required',
//        ]);
        $user = new User();
        $user->name = $request->username;
        $user->location_id = $request->location;
        $user->email = $request->email;
        $user->hourly_wage = $request->hourly_wage;
        $user->password = Hash::make($request->password);
        $user->save();
        $user->locationPolicies()->sync($request->permission);
        $user->roles()->sync($request->role);
        return redirect()->back()->with(['message'=>'user Successfully Created.']);

    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user->load('roles');
//        $isAllowed = MyCheck::check($user, 'Edit User Url', $user->location_id);
//        if($isAllowed) {
         $user->load('locationPolicies','roles');
         $locations = Location::with('policies')->get();
         $user_location_id= $user->locationPolicies->pluck('policy_id');
        $models = ['App\User','App\Orlando','App\Permission','App\Role','App\Location'];
        $roles = Role::all();
        return view('user.edit',compact('roles','locations','models','user','user_location_id'));
//        }else{
//            return redirect(route('dashboard'))->with(['Message','You Dont have Permission to access']);
//        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->name = $request->username;
        $user->location_id = $request->location;
        $user->email = $request->email;
        $user->hourly_wage = $request->hourly_wage;
        $user->locationPolicies()->sync($request->permission);

//        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->back()->with(['message'=>'user Successfully Updated.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user=Auth::user();
        $user->load('roles');
        $isAllowed = MyCheck::check($user, 'Delete User Url', $user->location_id);
        if($isAllowed) {
            $user->delete();
            return redirect()->back()->with(['message' => 'user Successfully Deleted.']);
        }else{
            return redirect(route('dashboard'))->with(['Message','You Dont have Permission to access']);
        }
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->filled('password'))
            $user->password = bcrypt($request->password);
        $user->save();
        return redirect(route('profileview'))->with(['message' => 'user Successfully Updated.']);
    }

    public function showProfile()
    {
        $user = Auth::User();
        return view('user.profile', compact('user'));
    }
}
