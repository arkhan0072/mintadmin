<?php

namespace App\Http\Controllers;

use App\Payroll;
use App\Payroll_adjustments;
use App\Payroll_snapshots;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class payrollController extends Controller
{
    public function import(Request $request)
    {

        if ($request->hasFile('importFile')) {

            $request->session()->put('location_id', 2);// set this value and get it from session

            $location_id = $request->session()->get('location_id');

            $users = DB::table('users')
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                ->join('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.name as username')
                ->whereRaw("LCASE(roles.name) = 'field staff'")
                ->where('users.location_id', $location_id)->get();


            $existing_users = [];
            foreach ($users as $user){
                array_push($existing_users, trim($user->username) );
            }



            $tmpName = $_FILES['importFile']['tmp_name'];
            $csvAsArray = new \SplFileObject($tmpName);
            $csvAsArray->setFlags(\SplFileObject::READ_CSV);

            $csvFile = new \SplFileObject($tmpName);
            $csvFile->seek(PHP_INT_MAX);
            $total_rows = $csvFile->key(); // get total rows in CSV


            $index = 0;
            $stopAt = $total_rows - 1; // skip the last line

            $not_exists = [];
            $not_exists_count = 0;
            foreach ($csvAsArray as $csvLine) {

                if ($index != 0 && $index < $stopAt) {

                    if ($this->preTest($csvLine)) {

                        // check for those users who does not exists in system. and skip those rows.
                        $str = trim($csvLine[8]);
                        $str = str_replace(",","%",$str);
                        $str = str_replace("and","%",$str);
                        $str = str_replace("And","%",$str);
                        $str = str_replace("%%","%",$str);
                        $str = str_replace("% %","% ",$str);

                        $str = preg_replace('/\s\s+/', ' ', $str); //remove more than 1 spaces.

                        $names = explode("%", $str);
                        $should_insert = false;
                        foreach ($names as $name){

                            if (!in_array(trim($name), $existing_users)){
                                if (!in_array(trim($name), $not_exists)) {
                                    array_push($not_exists, trim($name));
                                    $not_exists_count++;
                                }
                            }
                            else{
                                $should_insert = true;
                            }
                        }

                        if ($should_insert) {

                            $list = explode(":", $csvLine[9]);
                            $job_id = trim($list[0]);
                            $job_id = str_replace('#', '', $job_id);

                            $orlando = Payroll::create(
                                [
                                    'p_date' => date("Y-m-d", strtotime(trim($csvLine[0]))),
                                    'p_times' => trim($csvLine[1]),
                                    'completed' => trim($csvLine[2]),
                                    'client_name' => trim($csvLine[3]),
                                    'address' => trim($csvLine[4]),
                                    'city' => trim($csvLine[5]),
                                    'state' => trim($csvLine[6]),
                                    'zip_code' => trim($csvLine[7]),
                                    'assigned_to' => trim($csvLine[8]),
                                    'job_details' => trim($csvLine[9]),
                                    'job_id' => $job_id,
                                    'line_items' => trim($csvLine[10]),
                                    'duration' => trim($csvLine[11]),
                                    'one_off_job' => trim($csvLine[12]),
                                    'visit_based' => trim($csvLine[13]),
                                    'phone_no' => trim($csvLine[14]),
                                    'client_email' => trim($csvLine[15]),
                                    'location_id' => $location_id
                                ]);
                        }

                    }

                }
                $index++;
            }
            return redirect()->back()->with('message', 'Data imported Successfuly!')->with('not_exists',$not_exists)->with('not_exists_count', $not_exists_count);

        }

        return redirect()->back()->with('message', 'You\'re not allowed to visit here!');

    }

    function preTest($csvLine)
    {

        return empty(Payroll::where('p_date', date("Y-m-d", strtotime(trim($csvLine[0]))))->where('p_times', trim($csvLine[1]))->where('assigned_to', trim($csvLine[8]))->where('job_details', trim($csvLine[9]))->where('location_id', session()->get('location_id'))->first());
    }

    public function payroll_import(){
//        dd(session()->all());
        return view('payroll.import');
    }

    public function visits_report(){

        $data = $users = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.*','users.name as username','role_user.*','roles.*')
        ->where('users.location_id', session()->get('location_id'));


        return view('payroll.view_visits_report', ['users'=> $data->get()]);
    }

    public function view_payroll(Request $request, $id){

        $view_data = [];

        $view_data['filter'] = false;
        if ($request->session()->has('payrollFromDate')){
            $view_data['filter'] = true;
        }
        $view_data['from_date'] = $request->session()->pull('payrollFromDate', date("Y-m-d", strtotime('-15 days')));
        $view_data['to_date'] = $request->session()->pull('payrollToDate', date("Y-m-d"));



        $data =  DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.*','users.name as username','role_user.*','roles.*')
            ->where('users.id', $id)
            ->where('users.location_id', session()->get('location_id'));

//            dd($data->get());

        return view('payroll.view_payroll', ['user'=> $data->get()[0], 'view_data' => $view_data]);
    }

    public function employee_detail(){
        $data = $users = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.*','users.name as username','role_user.*','roles.*')
            ->whereRaw('LCASE(roles.name) = "field staff"')
            ->where('users.location_id', session()->get('location_id'));


        return view('payroll.employee_detail', ['users'=> $data->get()]);
    }

    public function view_payroll_details($id, $payroll_id = null){
        $data = [];
        $emp_id = $id;
        $data['emp_id'] = $emp_id;
        $data['payroll_snapshots'] =  DB::table('payroll_snapshots')
            ->where('emp_id', $id)->get();

        $data['snapshot_details'] = [];
        $data['payroll_snapshot'] = [];
        $data['user'] = DB::table('users')
            ->select('users.*', 'users.name as username')
            ->where('users.id', $emp_id)->get()[0];

        if ($payroll_id!=null){

            $pr_ss = DB::table('payroll_snapshots')
                ->where('id', $payroll_id)->get()[0];
            $data['payroll_snapshot'] = $pr_ss;
            $from_date = $pr_ss->from_date;
            $to_date = $pr_ss->to_date;
            $emp_id = $pr_ss->emp_id;

            $user =  $data['user'];

            $query = "SELECT p.*, (SELECT SUM(duration) FROM payrolls WHERE LCASE(assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 0 AND p_date BETWEEN '".$from_date."' AND '".$to_date."' AND location_id = '".session()->get('location_id')."' ) as work_hours
                                                FROM payrolls p
                                                WHERE LCASE(p.assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 0 AND p_date BETWEEN '".$from_date."' AND '".$to_date." AND location_id = '".session()->get('location_id')."'

                                            ";

            $data['snapshot_details'] = DB::select($query);

        }

        return view('payroll.view_payroll_details', ['data'=> $data]);
    }

    public function updatePayrollBankTransactionStatus(Request $request){
        $ps = new Payroll_snapshots();
//        if( $request->bank_transaction ) {
//            $bank_transaction = 1;
//        }
//        else{
//            $bank_transaction = 0;
//        }


//        $ps->where('id', $request->ps_id)->update(['bank_transaction' => $bank_transaction]);
        $ps->where('id', $request->ps_id)->update(['paid_via' => $request->payVia]);

        return redirect()->back();
    }

    public function ajaxApplyPayrollFilter(Request $request){

        $request->session()->put('payrollFromDate', $request->input('from_date'));
        $request->session()->put('payrollToDate', $request->input('to_date'));

        echo "ok";
    }

    public function ajax_payroll_adjustments_save(Request $request){


        //save all adjustments..
//        foreach ($request->amount as $key => $value){
//
//            $payroll = new Payroll_adjustments();
//            $payroll->emp_id = $request->emp_id;
//            $payroll->amount = $request->amount[$key];
//            $payroll->description = $request->description[$key];
//            $payroll->adjustment_date = $request->adjustment_date[$key];
//
//            $payroll->save();
//
//        }

        // now mark payroll entries as paid..
        $emp_name =  DB::table('users')->select('users.name as username')->where('users.id', $request->emp_id)->get()[0]->username;

        $payroll = new Payroll();
        $payroll->whereRaw("LCASE(assigned_to) LIKE '%".strtolower($emp_name)."%'")
            ->where('completed', 'Yes')
            ->where('is_paid', 0)
            ->whereBetween('p_date', [$request->adjFromDate, $request->adjToDate])
            ->update(['is_paid' => 1]);

        // now save a snapshot of current made payroll.
        $payroll_snapshot = new Payroll_snapshots();
        $payroll_snapshot->emp_id = $request->emp_id;
        $payroll_snapshot->from_date = $request->adjFromDate;
        $payroll_snapshot->to_date = $request->adjToDate;
//        $payroll_snapshot->bank_transaction = $request->bank_transaction;
        $payroll_snapshot->paid_via = $request->payVia;
        $payroll_snapshot->save();


        return redirect()->back();

    }

    public function payroll_adjustments_save(Request $request){


        //save all adjustments..
        foreach ($request->amount as $key => $value){

            $payroll = new Payroll_adjustments();
            $payroll->emp_id = $request->emp_id;
            $payroll->amount = $request->amount[$key];
            $payroll->description = $request->description[$key];
            $payroll->adjustment_date = $request->adjustment_date[$key];

            $payroll->save();

        }

        if ($request->adjFromDate != null || $request->adjFromDate != ''){
            $request->session()->put('payrollFromDate', $request->adjFromDate);
            $request->session()->put('payrollToDate', $request->adjToDate);
        }

        return redirect()->back()->with('msg', 'Adjustments Saved!');

    }

    public function payViaPayPal(Request $request){
// now mark payroll entries as paid..
        $emp =  DB::table('users')->select('users.*')->where('users.id', $request->emp_id)->get()[0];

        $name = $emp->name;
        $paypal_email = $emp->paypal_email;

        $enableSandbox = true;

        $data = [];
        $paypalConfig = [
            'email' => $paypal_email,
            'return_url' => 'http://127.0.0.1:8000/paypal/complete_transaction/'.base64_encode($request->adjFromDate).'/'.base64_encode($request->adjToDate).'/'.base64_encode($request->emp_id).'/'.base64_encode($request->payVia),
            'cancel_url' => 'http://127.0.0.1:8000/paypal/canceled',
            'notify_url' => 'http://127.0.0.1:8000/paypal/capture_ipn'
        ];

        $paypalUrl = $enableSandbox ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr';

        // Product being purchased.
        $itemName ="Payout for ".$name.' ('.$request->adjFromDate.' - '.$request->adjToDate.')';
        $itemAmount = $request->total_amount;

        // Set the PayPal account to whom i want to pay..
        $data['business'] = $paypalConfig['email'];
        // 'king.master127@gmail.com';
        //'asad@minthousecleaning.com';

        $data['first_name'] = $name;
        $data['last_name'] = '';
        $data['payer_email'] = $paypalConfig['email']; // receiver email probably
//        $data['item_number'] = '111';
//        $data['item_name'] = 'Test Item';
//        $data['item_amount'] = '10';
        $data['cmd'] = '_xclick';
        $data['no_note'] = '1';
        $data['lc'] = 'UK';
// $data['bn'] = 'PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest';
// Set the PayPal return addresses.
        $data['return'] = stripslashes($paypalConfig['return_url']);
        $data['cancel_return'] = stripslashes($paypalConfig['cancel_url']);
        $data['notify_url'] = stripslashes($paypalConfig['notify_url']);

        // Set the details about the product being purchased, including the amount
        // and currency so that these aren't overridden by the form data.
        $data['item_name'] = $itemName;
        $data['amount'] = $itemAmount;
        $data['currency_code'] = 'USD';

        // Add any custom fields for the query string.
        //$data['custom'] = USERID;

        // Build the query string from the data.
        $queryString = http_build_query($data);

        $paypal_link = $paypalUrl . '?' .$queryString;

        return redirect()->to($paypal_link);
    }

    public function complete_transaction($fd, $td, $ei, $pv){

        $from_date = base64_decode($fd);
        $to_date = base64_decode($td);
        $emp_id = base64_decode($ei);
        $paid_via = base64_decode($pv);

        $emp_name =  DB::table('users')->select('users.name as username')->where('users.id', $emp_id)->get()[0]->username;

                $payroll = new Payroll();
        $payroll->whereRaw("LCASE(assigned_to) LIKE '%".strtolower($emp_name)."%'")
            ->where('completed', 'Yes')
            ->where('is_paid', 0)
            ->whereBetween('p_date', [$from_date, $to_date])
            ->update(['is_paid' => 1]);

        // now save a snapshot of current made payroll.
        $payroll_snapshot = new Payroll_snapshots();
        $payroll_snapshot->emp_id = $emp_id;
        $payroll_snapshot->from_date = $from_date;
        $payroll_snapshot->to_date = $to_date;
//        $payroll_snapshot->bank_transaction = $request->bank_transaction;
        $payroll_snapshot->paid_via = $paid_via;
        $payroll_snapshot->save();
        return redirect()->to('view_payroll_details/'.$emp_id)->with('message', 'Succesfully Paid!');

    }

    public function ajaxVistsReport(){
//        $data = $users = DB::table('payroll')
//            ->select('payroll.*','payroll.assigned_to as username')
//            ->where('payroll.assigned_to', 'like', '');


//        $query = "SELECT u.*, p.*
//            FROM users u
//            LEFT JOIN payrolls p ON u.name LIKE concat('%',LCASE(p.assigned_to),'%')
//
//        ";

//        $query = "SELECT * FROM users WHERE ";
//        $data = DB::select($query);

        $data = $users = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('users.*','users.name as username','role_user.*','roles.*');

        dd($data->get());
//        return $data;
        return DataTables::of($data)
            ->make(true);
    }


}
