<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function policies(){
        return $this->belongsToMany('App\Policy')->withPivot('id');
    }

}
