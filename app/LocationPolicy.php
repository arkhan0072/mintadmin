<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationPolicy extends Model
{
    protected $table = 'location_policy';

    public function policy(){
        return $this->belongsTo('App\Policy');
    }
}
