<?php

namespace App;
use Carbon;
use Illuminate\Database\Eloquent\Model;

class Orlando extends Model
{


    protected $fillable = ['client_name', 'date', 'type', 'total', 'note', 'check_', 'invoice', 'method', 'transaction_id', 'employee', 'received', 'notes','location_id'];

}
