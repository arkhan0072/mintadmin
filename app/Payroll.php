<?php

namespace App;
use Carbon;
use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{


    protected $fillable = [
        'p_date',
        'p_times',
        'completed',
        'client_name',
        'address',
        'city',
        'state',
        'zip_code',
        'assigned_to',
        'job_details',
        'job_id',
        'line_items',
        'duration',
        'one_off_job',
        'visit_based',
        'phone_no',
        'client_email',
        'location_id'
    ];

}
