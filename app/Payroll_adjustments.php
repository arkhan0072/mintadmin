<?php

namespace App;
use Carbon;
use Illuminate\Database\Eloquent\Model;

class Payroll_adjustments extends Model
{


    protected $fillable = [
        'emp_id',
        'amount',
        'description',
        'adjustment_date'
    ];

}
