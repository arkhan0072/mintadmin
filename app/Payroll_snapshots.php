<?php

namespace App;
use Carbon;
use Illuminate\Database\Eloquent\Model;

class Payroll_snapshots extends Model
{


    protected $fillable = [
        'emp_id',
        'from_date',
        'to_date',
        'bank_transaction',
        'paid_via'
    ];

}
