<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    public function locations(){
        return $this->belongsToMany('App\Location');
    }
}
