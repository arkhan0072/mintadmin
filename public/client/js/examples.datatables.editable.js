(function($) {

	'use strict';

	var EditableTable = {

		options: {
			table: '#transactions'
		},

		initialize: function() {
			this
				.setVars()
				.build()
				.events();
		},

		setVars: function() {
			this.$table				= $( this.options.table );
			return this;
		},

		build: function() {
			this.datatable = table;

			window.dt = this.datatable;

			return this;
		},

		events: function() {
			var _self = this;

			this.$table
				.on('click', 'a.save-row', function( e ) {
					e.preventDefault();

					_self.rowSave( $(this).closest( 'tr' ) );
				})
				.on('click', 'a.cancel-row', function( e ) {
					e.preventDefault();

					_self.rowCancel( $(this).closest( 'tr' ) );
				})
				.on('click', 'a.edit-row', function( e ) {
					e.preventDefault();

					_self.rowEdit( $(this).closest( 'tr' ) );
				});
			return this;
		},

		// ==========================================================================================
		// ROW FUNCTIONS
		// ==========================================================================================
		rowCancel: function( $row ) {
			var _self = this, $actions, i, data;

			data = this.datatable.row( $row.get(0) ).data();
			this.datatable.row( $row.get(0) ).data( data );

			$actions = $row.find('td.action_ed_col');
			if ( $actions.get(0) ) {
				this.rowSetActionsDefault( $row );
			}
			this.datatable.draw();
		},

		selectIfEquals: function (v1, v2) {
			return v1 == v2 ? 'selected' : '';
        },

		rowEdit: function( $row ) {
			var _self = this,
				data;

			data = this.datatable.row( $row.get(0) ).data();

			$row.children( 'td' ).each(function( i ) {
				var $this = $( this );

				if ( $this.hasClass('action_ed_col') ) {
					_self.rowSetActionsEditing( $row );
				} else if($this.hasClass('rec_col')){
					$this.html( '<select  class="input-block"><option value="Y" '+_self.selectIfEquals('Y', data[i - 1])+'>Y</option><option value="N" '+ _self.selectIfEquals('N', data[i - 1]) +'>N</option></select>' );
				}else {
                    $this.html( '<input type="text" class="form-control input-block" value="' + data[i - 1] + '"/>' );
                }
			});
		},

		rowSave: function( $row ) {
			var _self     = this,
				$actions,
				values    = [], data;

            data = this.datatable.row( $row.get(0) ).data();

			values = $row.find('td').map(function() {
				var $this = $(this);

				if ( $this.hasClass('action_ed_col') ) {
					_self.rowSetActionsDefault( $row );
					return _self.datatable.cell( this ).data();
				} else if($this.hasClass('rec_col')) {
                    var v = $this.find('select').val();
                    console.log(v);
                    return $.trim( v );
				}else {
                    return $.trim( $this.find('input').val() );
				}
			});

            var startingIndex = 13;
            for(var index = 0; index < 12; index++){
            	values[index] = index == 3 ? values[index + startingIndex].replace('\$', '').replace(',', '') : values[index + startingIndex];
            	if(data[index]){
                    data[index] = values[index];
				}
			}

			//console.log(values);

            $.ajax({
                type: "POST",
                url: 'server/update-transaction.php',
                data: {'0': values[0], '1': values[1], '2': values[2], '3': values[3], '4': values[4], '5': values[5],
					'6': values[6], '7': values[7], '8': values[8], '9': values[9], '10': values[10], '11': values[11],
					'12': parseInt(values[12].replace('row_', ''))
				},
                success: function(data) {
					//console.log(data);
                }
            });

			this.datatable.row( $row.get(0) ).data( data );

			$actions = $row.find('td.action_ed_col');
			if ( $actions.get(0) ) {
				this.rowSetActionsDefault( $row );
			}

			this.datatable.draw();
		},

		rowSetActionsEditing: function( $row ) {
			$row.find( '.on-editing' ).removeClass( 'hidden' );
			$row.find( '.on-default' ).addClass( 'hidden' );
		},

		rowSetActionsDefault: function( $row ) {
			$row.find( '.on-editing' ).addClass( 'hidden' );
			$row.find( '.on-default' ).removeClass( 'hidden' );
		}

	};

	$(function() {
		EditableTable.initialize();
	});

}).apply(this, [jQuery]);