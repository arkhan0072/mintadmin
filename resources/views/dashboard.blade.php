@extends('master')

@section('location')

    @foreach($locations as $location)
        <a class="dropdown-item" onClick="sessionStorage.id={{$location->id}}" data-id="{{$location->id}}"><span>{{$location->name}}</span></a>
    @endforeach
{{--    <button onclick="alert(sessionStorage.getItem('id'))">Check Session Value</button>--}}

@endsection
@section('content')
    <di class="row">
    @if(session()->has('Message'))
        <div class="alert alert-success">
            {{ session()->get('Message') }}
        </div>
    @endif
    </di>
@endsection
@section('script')
    <script>
        $('.dropdown-item').on('click', function () {
            var id=$(this).data('id');
            alert(id);
            $.ajax({
                type: "get",
                url: '{{route('paymentdata')}}',
                data: {
                    location: id
                },
            })
            table.draw();
        });
    </script>
@endsection

