@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Users Details</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form action="{{route('form.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="col-md-6">
                                        <input class="form-control" name="formname" type="text"  value="{{ old('formname') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Amount</label>
                                    <div class="col-md-6">
                                        <input class="form-control" name="amount" type="text"  value="{{ old('amount') }}">
                                    </div>
                                </div>

                                <input type="submit" name="Create New form/link" class="btn btn-lg btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
