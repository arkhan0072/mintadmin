@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Users Details</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="data" class="table table-bordered" style="width:100%">
                                <thead>
                                <tr class="bg-pale-dark">
                                    <th>id</th>
                                    <th>name</th>
                                    <th>url</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {


            var methodes;
            table = $('#data').DataTable({
                dom: 'lBtip',

                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,

                ajax: {
                    url: '{{route('ajaxForms')}}',
                },
                columns: [
                    {data: 'id', name: 'id', orderable: false},
                    {data: 'name', name: 'Name', orderable: false},
                    {data: 'url', name: 'url', orderable: false},
                ],

            });
        });




    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
    <script>
        var clipboard = new ClipboardJS('.btn');

        clipboard.on('success', function(e) {
            console.info('Action:', e.action);
            alert('Link Copied: '+ e.text);
            console.info('Text:', e.text);
            console.info('Trigger:', e.trigger);

            e.clearSelection();
        });

        clipboard.on('error', function(e) {
            console.error('Action:', e.action);
            console.error('Trigger:', e.trigger);
        });

    </script>
@endsection


