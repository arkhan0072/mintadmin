<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 21 Oct 2019 06:20:12 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Veltrix - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}"><!--Chartist Chart CSS -->
    <link rel="stylesheet" href="{{asset('plugins/chartist/css/chartist.min.css')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{asset('client/css/dataTables.colResize.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/datatables.css')}}">
    <style>

        .preformatted {
            font-family: monospace;
            white-space: pre;
        }
        * {
            font-family: "Helvetica Neue", Helvetica;
            font-size: 15px;
            font-variant: normal;
            padding: 0;
            margin: 0;
        }

        html {
            height: 100%;
        }

        body {
            background: #E6EBF1;
            display: flex;
            align-items: center;
            justify-content: center;
            min-height: 100%;
        }

        form {
            width: 480px;
            margin: 20px 0;
        }

        .group {
            background: white;
            box-shadow: 0 7px 14px 0 rgba(49, 49, 93, 0.10),
            0 3px 6px 0 rgba(0, 0, 0, 0.08);
            border-radius: 4px;
            margin-bottom: 20px;
        }

        label {
            position: relative;
            color: #8898AA;
            font-weight: 300;
            height: 40px;
            line-height: 40px;
            margin-left: 20px;
            display: flex;
            flex-direction: row;
        }

        .group label:not(:last-child) {
            border-bottom: 1px solid #F0F5FA;
        }

        label > span {
            width: 80px;
            text-align: right;
            margin-right: 30px;
        }

        .field {
            background: transparent;
            font-weight: 300;
            border: 0;
            color: #31325F;
            outline: none;
            flex: 1;
            padding-right: 10px;
            padding-left: 10px;
            cursor: text;
        }

        .field::-webkit-input-placeholder {
            color: #CFD7E0;
        }

        .field::-moz-placeholder {
            color: #CFD7E0;
        }

        button {
            float: left;
            display: block;
            background: #666EE8;
            color: white;
            box-shadow: 0 7px 14px 0 rgba(49, 49, 93, 0.10),
            0 3px 6px 0 rgba(0, 0, 0, 0.08);
            border-radius: 4px;
            border: 0;
            margin-top: 20px;
            font-size: 15px;
            font-weight: 400;
            width: 100%;
            height: 40px;
            line-height: 38px;
            outline: none;
        }

        button:focus {
            background: #555ABF;
        }

        button:active {
            background: #43458B;
        }

        .outcome {
            float: left;
            width: 100%;
            padding-top: 8px;
            min-height: 24px;
            text-align: center;
        }

        .success, .error {
            display: none;
            font-size: 13px;
        }

        .success.visible, .error.visible {
            display: inline;
        }

        .error {
            color: #E4584C;
        }

        .success {
            color: #666EE8;
        }

        .success .token {
            font-weight: 500;
            font-size: 13px;
        }

    </style>
</head>
<body><!-- Begin page -->
<div id="wrapper">

<center>

        <style>

        </style>
        <img src="{{asset('assets/images/logo.png')}}" class="img-thumbnail">
        <p class="preformatted"><small>please input the billing information in the secure form bellow to
                procced with your booking process the agent will be notified upon
                this completion and you will recieve the booking confirmation </small>
        </p>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
        <form action="{{route('payment')}}" method="post">
            @csrf
            <div class="form-row align-items-center">
{{--                <div class="col-auto">--}}
{{--                    <label class="sr-only" for="inlineFormInput">Name</label>--}}
{{--                    <input type="text" class="form-control mb-2" id="stripeAmount" placeholder="Jane Doe">--}}
{{--                </div>--}}

                    <div class="form-group align-items-center">
                        <label class="text-center">Amount</label>
                        <input type="text" name="amount" class="form-control" id="stripeAmount" placeholder="Amount e.g 1 or 200..">
                    </div>

                <script
                    id="stripe-script"
                    src="https://checkout.stripe.com/checkout.js"
                    class="stripe-button"
                    data-image="{{asset('assets/images/logo.png')}}"
                    data-key="{{ \Setting::get('stripe_pk') }}"
                    data-name="Serendipity Artisan Blends"
                    data-description="Purchase Items"
                    data-amount="{{$form->amount * 100}}"
                >
                </script>

            </div>
        </form>
    @if(\Auth::user())
        <a href="{{route('home')}}" class="btn btn-primary">Go Back To Dashboard</a>
        <a href="{{route('paymentindex')}}" class="btn btn-primary">Go To Payments</a>
        @endif
    </center>
</div>

    <!-- jQuery  -->
    <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    {{--<script src="{{asset('assets/js/waves.min.js')}}"></script><!--Chartist Chart-->--}}
    {{--<script src="{{asset('plugins/chartist/js/chartist.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/chartist/js/chartist-plugin-tooltip.min.js')}}"></script><!-- peity JS -->--}}
    {{--<script src="{{asset('plugins/peity-chart/jquery.peity.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/pages/dashboard.js')}}"></script><!-- App js -->--}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script src="{{asset('assets/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets/js/waves.min.js')}}"></script><!-- Required datatable js -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script><!-- Buttons examples -->
    <script src="{{asset('plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.colVis.min.js')}}"></script><!-- Responsive examples -->
    <script src="{{asset('plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/responsive.bootstrap4.min.js')}}"></script><!-- Datatable init js -->
    <script src="{{asset('assets/pages/datatables.init.js')}}"></script><!-- App js -->
    <script src="{{asset('assets/js/app.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://js.stripe.com/v3/"></script>
    <script>

        $('.stripe-button-el').on('click', function () {
            var amount = $("#stripeAmount").val();
            if (amount == '') {
                amount= 1;
                $("#stripeAmount").val(amount)
            } else {
                amount;
                $("#stripeAmount").val(amount)
            }
            var token = function (res) {
                console.log(res);
                var $id = $('<input type=hidden name=stripeToken />').val(res.id);
                $('form').append($id).submit();
            };


            StripeCheckout.open({
                key: '{{ \Setting::get('stripe_pk') }}',
                amount: amount * 100,
                name: 'Mint Admin',
                image: '{{asset('assets/images/logo.png')}}',
                description: 'Cleaning Service',
                panelLabel: 'Checkout',
                token: token
            });

        })

    </script>
</body>
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 21 Oct 2019 06:21:11 GMT -->
</html>
