@extends('master')
@section('content')
@section('location')

    @foreach($locations as $location)
        <a class="dropdown-item" onClick="sessionStorage.id={{$location->id}}" data-id="{{$location->id}}"><span>{{$location->name}}</span></a>
    @endforeach
{{--    <button onclick="alert(sessionStorage.getItem('id'))">Check Session Value</button>--}}

@endsection
    <!--cards row-->
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Last Import</h5>
                        <h4 class="font-500"><span id="log"></span> <i class="fa fa-clock"></i> </h4>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white" style="min-height:122px;">
                <div class="card-body">
                    <div class="mb-4">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    </div>
                        <form action="{{ route('import') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                            <div class="col-md-8">
                            <input type="file" name="file" style="background: #626ed4!important; border: none;" class="form-control"/>
                            </div>
                            <div class="col-md-4">
                            <button class="btn btn-success">Import</button>
                            </div>
                            </div>
                        </form>
                </div>
                <div class="pt-2">
                    <div class="float-right">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4"><img
                                src="assets/images/services-icon/01.png" alt=""></div>
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Other</h5><h4
                            class="font-500">$ <span id="Other"></span></h4>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4"><img
                                src="assets/images/services-icon/04.png" alt=""></div>
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Uncollected Cash</h5><h4
                            class="font-500">$ <span id="totaluncolletedcash"></span></h4>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <!--cards row-->
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4"><img
                                src="assets/images/services-icon/01.png" alt=""></div>
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Uncollected Check</h5><h4
                            class="font-500">$ <span id="totaluncolletedcheck"></span></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4"><img
                                src="assets/images/services-icon/02.png" alt=""></div>
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Jobber Payment</h5><h4
                            class="font-500">$ <span id="Jobber_Payment"></span></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4"><img
                                src="assets/images/services-icon/03.png" alt=""></div>
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Credit Card</h5><h4
                            class="font-500">$ <span id="Credit_Card"></span></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-left mini-stat-img mr-4"><img
                                src="assets/images/services-icon/04.png" alt=""></div>
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Refunds</h5><h4
                            class="font-500">$ <span id="Refunds"></span></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">

        <div class="col-xl-3 col-md-6" id="is_between">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <h5 class="font-16 text-uppercase mt-0 text-white-50">Total Between</h5><h4
                            class="font-500">$ <span
                                id="totaluncolleted"></span></h4>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body"><h4 class="mt-0 header-title mb-4">Latest Trasaction</h4>
                    <div class="table-responsive">
                        <div class="d-flex">
                            <div class="form-group">
                                <label class="title">Payment Methods</label>
                                <select name="methods" id="methodz" class="form-control">
                                    <option value="">All</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Check">Check</option>
                                    <option value="Credit Card">Credit Card</option>
                                    <option value="Jobber Payments">Jobber Payments</option>
                                    <option value="Refund">Refunds</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                            <div class="form-group">
                                <label class="title">Payment Status</label>
                                <select name="payments" id="payments" class="form-control">

                                    <option value="">All</option>
                                    <option value="Y">Collected</option>
                                    <option value="N">Uncollected</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label class="title"> <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    Select Date Range to filter</label>
                                <div class="form-group d-flex">
                                    <input type="text" id="datepick" name="datepick" class="form-control" value=""/>
                                    &nbsp;&nbsp;&nbsp;
                                    <button class="cancelBtn btn btn-sm btn-info" name="reset" type="button">
                                        Reset
                                    </button>
                                </div>

                            </div>

                        </div>
                        <table id="data"  class="table-striped"
                               style="width: 100%;">
                            <thead>
                            <tr class="bg-pale-dark">
                                <th>Id</th>
                                <th>Client</th>
                                <th>Date</th>
                                <th>Type</th>
                                <th>Total</th>
                                <th>Note</th>
                                <th>Check</th>
                                <th>Invoice</th>
                                <th>Method</th>
                                <th>Transaction</th>
                                <th>Employee</th>
                                <th>Received</th>
                                <th>Notes</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" id="update_trans">
                                    @csrf
                                    <input type="hidden" name="id" id="update_id" value="">

                                    <div class="form-group d-flex">
                                        <div id="div_id_username" class="form-group required">
                                            <label for="id_username" class="control-label col-md-12  requiredField">
                                                Client Name<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <input class="input-md  textinput textInput form-control"
                                                       id="client_name" maxlength="90" value="" name="client_name"
                                                       style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_email" class="form-group required">
                                            <label for="id_email" class="control-label col-md-12  requiredField">
                                                Date<span class="asteriskField">*</span></label>
                                            <div class="controls col-md-12">
                                                <div class="input-group date" >
                                                    <input type="text" id="date" name="dates" value=""
                                                           class="form-control">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-th"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="div_id_password1" class="form-group required">
                                            <label for="id_password1" class="control-label col-md-12  requiredField">Type<span
                                                    class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <input class="input-md textinput textInput form-control" id="type"
                                                       name="type" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group d-flex">
                                        <div id="div_id_password2" class="form-group required">
                                            <label for="id_password2" class="control-label col-md-12  requiredField">
                                                Total <span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <input class="input-md textinput textInput form-control" id="total"
                                                       name="total" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>

                                        <div id="div_id_name" class="form-group required">
                                            <label for="id_name" class="control-label col-md-12  requiredField">
                                                Invoice<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <input class="input-md textinput textInput form-control" id="invoice"
                                                       name="invoice" value=""
                                                       style="margin-bottom: 10px">
                                            </div>
                                        </div>



                                        <div id="div_id_company" class="form-group required">
                                            <label for="id_company" class="control-label col-md-12  requiredField">
                                                Method<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <input class="input-md textinput textInput form-control" id="method"
                                                       name="methods" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group d-flex">


                                        <div id="div_id_name" class="form-group required">
                                            <label for="id_name" class="control-label col-md-12  requiredField">
                                                Note<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <textarea class="input-md textinput textInput form-control" id="note"
                                                          name="note" value="" style="margin-bottom: 10px"></textarea>
                                            </div>
                                        </div>

                                        <div id="div_id_name" class="form-group required">
                                            <label for="id_name" class="control-label col-md-12 requiredField">
                                                Check_<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <textarea class="input-md textinput textInput form-control" id="check"
                                                          name="check" value="" style="margin-bottom: 10px"></textarea>
                                            </div>
                                        </div>

                                        <div id="div_id_location" class="form-group required">
                                            <label for="id_location" class="control-label col-md-12  requiredField">
                                                Notes<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12 ">
                                                <textarea class="input-md textinput textInput form-control" id="notes"
                                                          name="notes" value="" style="margin-bottom: 10px"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <div id="div_id_catagory" class="form-group required">
                                        <label for="id_catagory" class="control-label col-md-6  requiredField">
                                            Transaction<span class="asteriskField">*</span> </label>
                                        <div class="controls col-md-12">
                                            <input class="input-md textinput textInput form-control"
                                                   id="transaction" name="transaction" value=""
                                                   style="margin-bottom: 10px" type="text">
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group d-flex">
                                        <div id="div_id_catagory" class="form-group required">
                                            <label for="id_catagory" class="control-label col-md-12  requiredField">
                                                Employee<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <input class="input-md textinput textInput form-control" id="employee"
                                                       name="employee" value="" style="margin-bottom: 10px" type="text">
                                            </div>
                                        </div>
                                        <div id="div_id_location" class="form-group required">
                                            <label for="id_location" class="control-label col-md-12  requiredField">
                                                Received<span class="asteriskField">*</span> </label>
                                            <div class="controls col-md-12">
                                                <select class="input-md textinput textInput form-control" id="received"
                                                       name="received">
                                                    <option value="N">N</option>
                                                    <option value="Y">Y</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <!--<button type="button" class="btn btn-default close" data-dismiss="modal">-->
                                        <!--    Close-->
                                        <!--</button>-->
                                        <button type="submit" class="btn btn-primary update-trans">Update</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).on('click','.btn-primary',function () {

                id = $(this).data('id');
                $.ajax({
                    type: "get",
                    url: '{{route('editOrlando')}}',
                    data: {
                        id: id
                    },
                    success: function (response) {
                        $('#update_id').val(response.id);
                        $('#client_name').val(response.client_name);
                        $('input[name="dates"]').daterangepicker({
                            singleDatePicker: true,
                            format: 'mm/dd/yyyy',
                        });
                        $('#date').val(response.date);
                        $('#type').val(response.type);
                        $('#total').val(response.total);
                        $('#note').val(response.note);
                        $('#check').val(response.check_);
                        $('#invoice').val(response.invoice);
                        $('#method').val(response.method);
                        $('#transaction').val(response.transaction_id);
                        $('#employee').val(response.employee);
                        $('#received').val(response.received);
                        $('#notes').val(response.notes);
                        console.log(response);
                    }

                });

        });
        var table;
        var between;
        var id;
        $(document).ready(function () {

            $('#data thead tr').clone(true).appendTo('#data thead').attr('id', 'tr2');

            $('#data thead tr:eq(1) th').each(function (i) {
                if (i === $('#data thead tr:eq(1) th').length ) {
                    return;
                }
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {

                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });


            var methodes;
            table = $('#data').DataTable({

                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,
                responsive: false,
                scrollX: true,
                bStateSave: true,
                ajax: {
                    url: '{{route('paymentdata')}}',
                    data: function (d) {
                        d.methodz = $('#methodz :selected').val();
                        d.payments = $('#payments :selected').val();
                        d.datepick = $('#datepick').val();

                    }
                },
                columns: [
                    // {data: 'edit', Title: 'edit', orderable: false},
                    {data: 'id', Title: 'id', orderable: false},
                    {data: 'client_name', Title: 'Client', orderable: false},
                    {data: 'date', Title: 'Date', orderable: false},
                    {data: 'type', Title: 'Type', orderable: false},
                    {data: 'total', Title: 'Total', orderable: false},
                    {data: 'note', Title: 'Note', orderable: false},
                    {data: 'check_', Title: 'Check', orderable: false},
                    {data: 'invoice', Title: 'Invoice', orderable: false},
                    {data: 'method', Title: 'Method', orderable: false},
                    {data: 'transaction_id', Title: 'Transaction', orderable: false},
                    {data: 'employee', Title: 'Employee', orderable: false},
                    {data: 'received', Title: 'Received', orderable: false},
                    {data: 'notes', Title: 'Notes', orderable: false},
                ],
                buttons: [

                    {extend: 'print', className: 'btn dark btn-outline'},
                    {extend: 'copy', className: 'btn red btn-outline'},
                    {extend: 'pdf', className: 'btn green btn-outline'},
                    {extend: 'excel', className: 'btn yellow btn-outline '},
                    {extend: 'csv', className: 'btn purple btn-outline '},
                    {extend: 'colvis', className: 'btn btn-primary', text: 'Columns'}
                ],
                columnDefs: [

                    {
                        "width": "3px",
                        "targets": 0
                    },

                    {
                        "width": "43px",
                        "targets": 1
                    },
                    {
                        "width": "50px",
                        "targets": 2
                    },
                    {
                        "width": "45px",
                        "targets": 3,
                    },
                    {
                        "width": "37px",
                        "targets": 4
                    },
                    {
                        "width": "25px",
                        "targets": 5
                    },
                    {
                        "width": "38px",
                        "targets": 6
                    },
                    {
                        "width": "34px",
                        "targets": 7
                    },
                    {
                        "width": "36px",
                        "targets": 8
                    },
                    {
                        "width": "36px",
                        "targets": 9
                    },
                    {
                        "width": "15px",
                        "targets": 10
                    },
                    {
                        "width": "35px",
                        "targets": 11
                    },
                    {
                        "width": "120px",
                        "targets": 12
                    }],
            });


            $('#methodz').on('change', function () {
                table.draw();
            });
            $('#payments').on('change', function () {
                table.draw();
            });
            table.on('xhr', function () {
                var total;
                var json = table.ajax.json();
                var sum = JSON.parse(json.sum);


                var sum1= [];
                for (x in sum) {
                    sum1.push(sum[x]);
                }
                var i;
                console.log(sum1);

                total = sum1.reduce(function(total, x){
                    return total= total+parseFloat(x);
                }, 0);
                console.log(total);


                console.log(total);
                $('#log').html(json.logs);
                if(total){

                    $('#totaluncolleted').html(total);
                }else{
                    $('#totaluncolleted').html(0);
                }
                if(sum.hasOwnProperty('Cash')){
                    $('#totaluncolletedcash').html(sum["Cash"]);

                }else{
                    $('#totaluncolletedcash').html(0);
                }
                if(sum.hasOwnProperty('Check')){
                    $('#totaluncolletedcheck').html(sum["Check"]);
                }else{
                    $('#totaluncolletedcheck').html(0);
                }
                if(sum.hasOwnProperty('Credit Card')){
                    $('#Credit_Card').html(sum["Credit Card"]);
                }else{
                    $('#Credit_Card').html(0);
                }
                if(sum.hasOwnProperty('Jobber Payments')){
                    $('#Jobber_Payment').html(sum["Jobber Payments"]);
                }else{
                    $('#Jobber_Payment').html(0);
                }
                if(sum.hasOwnProperty('Refund')){
                    $('#Refunds').html(sum["Refund"]);
                }else{
                    $('#Refunds').html(0);
                }
                if(sum.hasOwnProperty('Other')){
                    $('#Other').html(sum["Other"]);
                }else{
                    $('#Other').html(0);
                }


            });
            /* Predefined Ranges */
            //
            // var start = moment().subtract('90','days');
            // var end = moment();

            function cb(start, end) {
                between = $('#datepick').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                table.draw();
            }


            // cb(start, end)


            $(function () {
                var start, end;
                $('input[name="datepick"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });
                $('input[name="datepick"]').on('apply.daterangepicker', function (ev, picker) {
                    $("#is_between").show();
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                    table.draw();
                });

                $('input[name="datepick"]').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                    table.draw();
                });
                //
                // $('#datepick').daterangepicker({
                //     startDate: start,
                //     endDate: end,
                //     ranges: {
                //         'Today': [moment(), moment()],
                //         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                //         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                //         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                //         'This Month': [moment().startOf('month'), moment().endOf('month')],
                //         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                //     }
                // },cb);

            });
            $('.cancelBtn').on('click', function () {
                $('#datepick').val();
                $('#payments').val();
                $('#methodz').val();
                location.reload();
            });


        });

        // window.addEventListener("load", function () {
        //     setTimeout(function () {
        //
        //     }, 5000);
        //
        // }, false);
        $('.update-trans').on('click', function (e) {
            e.preventDefault();

            var datas = $("#update_trans").serialize();
            $.ajax({
                type: "POST",
                url: '{{route('updateOrlando')}}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: $('#update_id').val(),
                    client_name: $('#client_name').val(),
                    date: $('#date').val(),
                    type: $('#type').val(),
                    total: $('#total').val(),
                    note: $('#note').val(),
                    check: $('#check').val(),
                    invoice: $('#invoice').val(),
                    methods: $('#method').val(),
                    transaction: $('#transaction').val(),
                    employee: $('#employee').val(),
                    received: $('#received').val(),
                    notes: $('#notes').val(),
                },
                success: function (response) {
                    console.log(response)
                    table.draw(false);
                    $('.close').click();
                }

            })
        });


        /*$('.applyBtn').on('click',function(){

            // table.destroy();
            table.draw(); // Redraw the DataTable
        });*/
        $("#is_between").hide();


        $('.dropdown-item').on('click', function () {
            var id=$(this).data('id');
            alert(id);
            $.ajax({
                type: "get",
                url: '{{route('paymentdata')}}',
                data: {
                    location: id
                },
            })
            table.draw();
        });
    </script>
@endsection

