﻿<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from crypto-admin-templates.multipurposethemes.com/src2/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 12 Oct 2019 13:20:23 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://crypto-admin-templates.multipurposethemes.com/images/favicon.ico">

    <title>Crypto Admin - Dashboard</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{asset('../assets/vendor_components/bootstrap/dist/css/bootstrap.css')}}">

    <!--amcharts -->
{{--    <link href="{{asset('../www.amcharts.com/lib/3/plugins/export/export.css')}}" rel="stylesheet" type="text/css" />--}}

    <!-- Bootstrap-extend -->
    <link rel="stylesheet" href="{{asset('css/bootstrap-extend.css')}}">

    <!-- theme style -->
    <link rel="stylesheet" href="{{asset('css/master_style.css')}}">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{asset('css/skins/_all-skins.css')}}">

    <link href="http://www.zainkhalid.website/assets/lib/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/datatables.min.css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">

  <!--add header here-->
  @include('investor2.includes/header')
  <!--end header here-->

  <!-- Left side column. contains the logo and sidebar -->
  @include('investor2.includes/sidebar')

  <!-- Content Wrapper. Contains page content -->
  @yield('content')

  <!-- /.content-wrapper -->

  @include('investor2.includes/footer')
</div>
<!-- ./wrapper -->



<!-- jQuery 3 -->
<script src="{{asset('../assets/vendor_components/jquery/dist/jquery.js')}}"></script>

<!-- popper -->
<script src="{{asset('../assets/vendor_components/popper/dist/popper.min.js')}}"></script>

<!-- Bootstrap 4.0-->
<script src="{{asset('../assets/vendor_components/bootstrap/dist/js/bootstrap.js')}}"></script>

<!-- Slimscroll -->
<script src="{{asset('../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- FastClick -->
<script src="{{asset('../assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>



<!-- This is data table -->
<script src="{{asset('../assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js')}}"></script>

<!-- Sparkline -->
<script src="{{asset('../assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- Crypto_Admin App -->
<script src="{{asset('js/template.js')}}"></script>

<!-- Crypto_Admin dashboard demo (This is only for demo purposes) -->
{{--<script src="{{asset('js/pages/dashboard.js')}}"></script>--}}
{{--<script src="{{asset('js/pages/dashboard-chart.js')}}"></script>--}}

<!-- Crypto_Admin for demo purposes -->
{{--<script src="{{asset('js/demo.js')}}"></script>--}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.20/b-1.6.0/b-colvis-1.6.0/b-flash-1.6.0/b-html5-1.6.0/b-print-1.6.0/datatables.min.js"></script>
<script src="http://www.zainkhalid.website/assets/lib/bootstrap-daterangepicker/moment.js"></script>
<script src="http://www.zainkhalid.website/assets/lib/bootstrap-daterangepicker/daterangepicker.js"></script>

@yield('script')
</body>

<!-- Mirrored from crypto-admin-templates.multipurposethemes.com/src2/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 12 Oct 2019 13:22:07 GMT -->
</html>
