<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Made by Weiser Agencies Ltd.
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{\Carbon\Carbon::now()->format('Y')}} <a href="#"></a>Pamela's Eatery</strong> All rights reserved.
</footer>