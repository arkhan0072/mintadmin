<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 21 Oct 2019 06:22:34 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Payment Control Login</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="home-btn d-none d-sm-block"><a href="index.html" class="text-dark"><i class="fas fa-home h2"></i></a></div>
<div class="wrapper-page">
    <div class="card overflow-hidden account-card mx-3">
        <div class="bg-primary p-4 text-white text-center position-relative"><h4 class="font-20 m-b-5">Welcome Back
                !</h4>
            <p class="text-white-50 mb-4">Sign in to continue to Veltrix.</p><a href="index.html"
                                                                                class="logo logo-admin"><img
                    src="assets/images/logo-sm.png" height="24" alt="logo"></a></div>
        <div class="account-card-content">
            @yield('content')
        </div>
    </div>
    <div class="m-t-40 text-center"><p>Don't have an account ? <a href="pages-register.html"
                                                                  class="font-500 text-primary">Signup now</a></p>
        <p>© 2019 Veltrix. Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand</p></div>
</div><!-- end wrapper-page --><!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/metisMenu.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/waves.min.js')}}"></script><!-- App js -->
<script src="{{asset('assets/js/app.js')}}"></script>
</body>
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 21 Oct 2019 06:22:34 GMT -->
</html>
