<button class="btn btn-danger" onclick="frmdlt{{$location->id}}.submit();"  title="Block"><i class="fa fa-ban"></i></button>
<form name="frmdlt{{$location->id}}" action="{{ route('location.destroy', $location->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
