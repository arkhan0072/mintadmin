@extends('master')
@section('content')
    <div class="card">
        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Create Location</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form action="{{route('location.update',$location->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="col-md-6">
                                        <input class="form-control" name="name" type="text"  value="{{ old('name',$location->name) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="address" value="{{ old('address',$location->address) }}">
                                    </div>
                                </div>

                                <input type="submit" class="btn btn-lg btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
