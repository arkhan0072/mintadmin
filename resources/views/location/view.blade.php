@extends('master')
@section('content')
<div class="card">

    <div class="row">
        <div class="col-md-12">
            <div class="card bg-gray-light ">
                <div class="card-header text-black anime">
                    <p class="text-black-50">Select Current Location</p><br>
                    <p class="text-black-50">Currently Selected: <b>{{$current_location}}</b></p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <form action="{{route('set_location')}}">
                            @csrf
                            @method('GET')
                            <div class="form-group">
                                <label>Select Current Location</label>
                                <div class="col-md-6">
                                    <select name="location" id="" class="form-control">
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-lg btn-primary">Save Location</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
