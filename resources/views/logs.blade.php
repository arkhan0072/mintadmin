@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-white anime">
                        Logs Overview
                        <p class="text-white">Log Details</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                        <table id="data" class="table table-bordered" style="width:100%">
                            <thead>
                            <tr class="bg-pale-dark">
                                <th>Id</th>
                                <th>File Name</th>
                                <th>Date</th>
                                <th>Create Time</th>
                                <th>Update Time</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {
            $('#data thead tr').clone(true).appendTo('#data thead').attr('id', 'tr2');

            $('#data thead tr:eq(1) th').each(function (i) {
                if (i === $('#data thead tr:eq(1) th').length ) {
                    return;
                }
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {

                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });


            var methodes;
            table = $('#data').DataTable({
                dom: 'lBftip',
                buttons: [
                    

                ],
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,
                responsive: true,
                scrollX: true,
                ajax: {
                    url: '{{route('getlogs')}}',
                },
                columns: [
                    {data: 'id', Title: 'id', orderable: false},
                    {data: 'file_name', Title: 'Client', orderable: false},
                    {data: 'date', Title: 'Date', orderable: false},
                    {data: 'created_at', Title: 'Create Time', orderable: false},
                    {data: 'updated_at', Title: 'Update Time', orderable: false},
                ],
                "columnDefs": [

                    {
                        "targets": 0
                    },

                    {
                        "targets": 1
                    },
                    {

                        "targets": 2
                    },
                    {

                        "targets": 3
                    },
                    {

                        "targets": 4
                    }
                ],
            });
            });




    </script>
@endsection


