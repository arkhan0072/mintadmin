<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 21 Oct 2019 06:20:12 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Mint Dashboard</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="Themesbrand" name="author">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}"><!--Chartist Chart CSS -->
    <link rel="stylesheet" href="{{asset('plugins/chartist/css/chartist.min.css')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/metismenu.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{asset('client/css/dataTables.colResize.css')}}">
    <link rel="stylesheet" href="{{asset('client/css/datatables.css')}}">
</head>
<body><!-- Begin page -->
<div id="wrapper"><!-- Top Bar Start -->
    <div class="topbar"><!-- LOGO -->
        <div class="topbar-left">

            <a href="{{route('home')}}">
                <h3><span style="color:#fff;">Mint </span> <span style="color:#0892d0;">Dashboard</span></h3>
            </a>

        </div>
        <nav class="navbar-custom">
            <ul class="navbar-right list-inline float-right mb-0">
                <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                    <!--<form role="search" class="app-search">-->
                    <!--    <div class="form-group mb-0"><input type="text" class="form-control" placeholder="Search..">-->
                    <!--        <button type="submit"><i class="fa fa-search"></i></button>-->
                    <!--    </div>-->
                    <!--</form>-->
                </li><!-- language-->
                @if(Auth::user()->roles[0]->id==1)
                    <li class="dropdown notification-list list-inline-item d-none d-md-inline-block"><a
                            class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#"
                            role="button" aria-haspopup="false" aria-expanded="false"><span><i class="fa fa-globe"></i> </span></a>
                        <div class="dropdown-menu dropdown-menu-right language-switch">
                        @yield('location')
                        {{--                                class="dropdown-item" href="#"><img src="assets/images/flags/french_flag.jpg" alt=""--}}
                        {{--                                                                    height="16"><span>French </span></a><a--}}
                        {{--                                class="dropdown-item" href="#"><img src="assets/images/flags/spain_flag.jpg" alt=""--}}
                        {{--                                                                    height="16"><span>Spanish </span></a><a--}}
                        {{--                                class="dropdown-item" href="#"><img src="assets/images/flags/russia_flag.jpg" alt=""--}}
                        {{--                                                                    height="16"><span>Russian</span></a></div>--}}
                    </li><!-- full screen -->
                @endif
                <li class="dropdown notification-list list-inline-item d-none d-md-inline-block"><a
                        class="nav-link waves-effect" href="#" id="btn-fullscreen"><i
                            class="mdi mdi-fullscreen noti-icon"></i></a></li><!-- notification -->
                {{--                <li class="dropdown notification-list list-inline-item"><a--}}
                {{--                            class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#"--}}
                {{--                            role="button" aria-haspopup="false" aria-expanded="false"><i--}}
                {{--                                class="mdi mdi-bell-outline noti-icon"></i> <span--}}
                {{--                                class="badge badge-pill badge-danger noti-icon-badge">3</span></a>--}}
                {{--                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg"><!-- item--><h6--}}
                {{--                                class="dropdown-item-text">Notifications (258)</h6>--}}
                {{--                        <div class="slimscroll notification-item-list"><!-- item--> <a href="javascript:void(0);"--}}
                {{--                                                                                       class="dropdown-item notify-item active">--}}
                {{--                                <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>--}}
                {{--                                <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span>--}}
                {{--                                </p></a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                {{--                                <div class="notify-icon bg-warning"><i class="mdi mdi-message-text-outline"></i></div>--}}
                {{--                                <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span>--}}
                {{--                                </p></a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                {{--                                <div class="notify-icon bg-info"><i class="mdi mdi-glass-cocktail"></i></div>--}}
                {{--                                <p class="notify-details">Your item is shipped<span class="text-muted">It is a long established fact that a reader will</span>--}}
                {{--                                </p></a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                {{--                                <div class="notify-icon bg-primary"><i class="mdi mdi-cart-outline"></i></div>--}}
                {{--                                <p class="notify-details">Your order is placed<span class="text-muted">Dummy text of the printing and typesetting industry.</span>--}}
                {{--                                </p></a><!-- item--> <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                {{--                                <div class="notify-icon bg-danger"><i class="mdi mdi-message-text-outline"></i></div>--}}
                {{--                                <p class="notify-details">New Message received<span class="text-muted">You have 87 unread messages</span>--}}
                {{--                                </p></a></div><!-- All--> <a href="javascript:void(0);"--}}
                {{--                                                             class="dropdown-item text-center text-primary">View all <i--}}
                {{--                                    class="fi-arrow-right"></i></a></div>--}}
                {{--                </li>--}}
                <li class="dropdown notification-list list-inline-item">
                    <div class="dropdown notification-list nav-pro-img"><a
                            class="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown"
                            href="#" role="button" aria-haspopup="false" aria-expanded="false"><img
                                src="assets/images/users/user-4.jpg" alt="user" class="rounded-circle"></a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown"><!-- item-->
                            <a
                                class="dropdown-item" href="{{route('profileview')}}"><i
                                    class="mdi mdi-account-circle m-r-5"></i> Profile</a>
                            {{--                            <a class="dropdown-item" href="#"><i class="mdi mdi-wallet m-r-5"></i> My Wallet</a>--}}
                            @if(Auth::user()->roles[0]->id==1)
                                <a class="dropdown-item d-block" href="{{route('settings.show')}}"><i
                                        class="mdi mdi-settings m-r-5"></i> Settings</a>
                            @endif
                            {{--                           <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5"></i> Lock screen</a>--}}
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-danger"
                               onclick="event.preventDefault();document.getElementById('logout_form').submit();"><i
                                    class="mdi mdi-power text-danger"></i>
                                Logout</a></div>
                        <form style="display:none;" id='logout_form' action="{{route('logout')}}" method="post">
                            {{csrf_field()}}
                        </form>
                    </div>
                </li>
            </ul>
            {{--            <ul class="list-inline menu-left mb-0">--}}
            {{--                <li class="float-left">--}}
            {{--                    <button class="button-menu-mobile open-left waves-effect"><i class="mdi mdi-menu"></i></button>--}}
            {{--                </li>--}}
            {{--                <li class="d-none d-sm-block">--}}
            {{--                    <div class="dropdown pt-3 d-inline-block"><a class="btn btn-light dropdown-toggle" href="#"--}}
            {{--                                                                 role="button" id="dropdownMenuLink"--}}
            {{--                                                                 data-toggle="dropdown" aria-haspopup="true"--}}
            {{--                                                                 aria-expanded="false">Create</a>--}}
            {{--                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item" href="#">Action</a>--}}
            {{--                            <a class="dropdown-item" href="#">Another action</a> <a class="dropdown-item" href="#">Something--}}
            {{--                                else here</a>--}}
            {{--                            <div class="dropdown-divider"></div>--}}
            {{--                            <a class="dropdown-item" href="#">Separated link</a></div>--}}
            {{--                    </div>--}}
            {{--                </li>--}}
            {{--            </ul>--}}
        </nav>
    </div><!-- Top Bar End --><!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="slimscroll-menu" id="remove-scroll"><!--- Sidemenu -->
            <div id="sidebar-menu"><!-- Left Menu Start -->
                <ul class="metismenu" id="side-menu">
                    <li class="menu-title">Main Navigation</li>

                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-clipboard"></i><span> Payment Control <span
                                    class="float-right menu-arrow"><i
                                        class="mdi mdi-chevron-right"></i></span></span></a>
                        <ul class="submenu">
                            @php
                                use App\Http\Controllers\MyCheck;
                                   $user=Auth::user();
                                   $user->load('roles');
                                   $isAllowed = MyCheck::check($user, 'View Home Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('home')}}"><i class="ti-home"></i> <span>Home</span></a></li>
                            @endif
                            @php
                                $user=Auth::user();
                                $user->load('roles');
                                $isAllowed = MyCheck::check($user, 'View Log', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('logs')}}"><i
                                        class="ti-file"></i><span> Logs</span></a></li>
                                @endif
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i> <span>Users<span
                                    class="float-right menu-arrow"><i
                                        class="mdi mdi-chevron-right"></i></span></span></a>
                        <ul class="submenu">
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'View User Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                                <li><a href="{{route('user.index')}}"><i
                                            class="ti-user"></i> View All Users</a></li>
                            @endif
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'Create User Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('user.create')}}"><i
                                        class="ti-plus"></i> Add New Users</a></li>
                                @endif
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i>
                            <span>Locations<span
                                    class="float-right menu-arrow"><i
                                        class="mdi mdi-chevron-right"></i></span></span></a>
                        <ul class="submenu">
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'View Location', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('location.index')}}"><i
                                        class="ti-user"></i> View All Lcations</a></li>
                            @endif
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'Create Location', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('location.create')}}"><i
                                        class="ti-plus"></i> Add New Location</a></li>
                                @endif
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-package"></i> <span>Permissions & Roles<span
                                    class="float-right menu-arrow"><i
                                        class="mdi mdi-chevron-right"></i></span></span></a>
                        <ul class="submenu">
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'Create Permission Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('permission.create')}}"><i
                                        class="ti-plus"></i> Create Permission</a></li>
                            @endif
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'View Permission Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('permission.index')}}"><i
                                        class="ti-eye"></i> View Permissions</a></li>
                            @endif
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'Create Role Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('role.create')}}"><i
                                        class="ti-plus"></i> Create Role</a></li>
                            @endif
                            @php
                                $user=Auth::user();
            $user->load('roles');
            $isAllowed = MyCheck::check($user, 'View Role Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('role.index')}}"><i class="ti-eye"></i> View Role</a></li>
                                @endif
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-gift"></i> <span>Customer Dealing<span
                                    class="float-right menu-arrow"><i
                                        class="mdi mdi-chevron-right"></i></span></span></a>
                        <ul class="submenu">
                            @php
                            $user=Auth::user();
                            $user->load('roles');
                            $isAllowed = MyCheck::check($user, 'Create Generator Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('form.create')}}"><i
                                        class="ti-user"></i>Generate payment Link</a></li>
                            @endif
                            @php
                                $user=Auth::user();
                                $user->load('roles');
                                $isAllowed = MyCheck::check($user, 'View Generator Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('form.index')}}"><i
                                        class="ti-eye"></i>View All payment Link</a></li>
                            @endif
                            @php
                                $user=Auth::user();
                                $user->load('roles');
                                $isAllowed = MyCheck::check($user, 'View Generator Url', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('form.show',1)}}"><i
                                        class="ti-eye"></i>form Free amount to Entere</a></li>
                            @endif
                            @php
                                $user=Auth::user();
                                $user->load('roles');
                                $isAllowed = MyCheck::check($user, 'View Payments', $user->location_id);
                            @endphp
                            @if($isAllowed)
                            <li><a href="{{route('paymentindex')}}"><i
                                        class="ti-eye"></i> view Payments</a></li>
                                @endif
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-money"></i> <span>Payroll<span
                                    class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>
                        <ul class="submenu">
                            <li><a href="{{route('payroll_import')}}"><i
                                        class="ti-file"></i> import Visits</a></li>
                            <li><a href="{{route('visits_report')}}"><i
                                        class="ti-view-list"></i>Summary</a></li>
                            <li><a href="{{route('employee_detail')}}"><i
                                        class="ti-user"></i>Employee Detail</a></li>
                        </ul>
                    </li>
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-receipt"></i><span> Forms <span--}}
                    {{--                                        class="badge badge-pill badge-success float-right">9</span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="form-elements.html">Form Elements</a></li>--}}
                    {{--                            <li><a href="form-validation.html">Form Validation</a></li>--}}
                    {{--                            <li><a href="form-advanced.html">Form Advanced</a></li>--}}
                    {{--                            <li><a href="form-editors.html">Form Editors</a></li>--}}
                    {{--                            <li><a href="form-uploads.html">Form File Upload</a></li>--}}
                    {{--                            <li><a href="form-xeditable.html">Form Xeditable</a></li>--}}
                    {{--                            <li><a href="form-repeater.html">Form Repeater</a></li>--}}
                    {{--                            <li><a href="form-wizard.html">Form Wizard</a></li>--}}
                    {{--                            <li><a href="form-mask.html">Form Mask</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i--}}
                    {{--                                    class="ti-pie-chart"></i><span> Charts <span class="float-right menu-arrow"><i--}}
                    {{--                                            class="mdi mdi-chevron-right"></i></span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="charts-morris.html">Morris Chart</a></li>--}}
                    {{--                            <li><a href="charts-chartist.html">Chartist Chart</a></li>--}}
                    {{--                            <li><a href="charts-chartjs.html">Chartjs Chart</a></li>--}}
                    {{--                            <li><a href="charts-flot.html">Flot Chart</a></li>--}}
                    {{--                            <li><a href="charts-knob.html">Jquery Knob Chart</a></li>--}}
                    {{--                            <li><a href="charts-echart.html">E - Chart</a></li>--}}
                    {{--                            <li><a href="charts-sparkline.html">Sparkline Chart</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i--}}
                    {{--                                    class="ti-view-grid"></i><span> Tables <span class="float-right menu-arrow"><i--}}
                    {{--                                            class="mdi mdi-chevron-right"></i></span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="tables-basic.html">Basic Tables</a></li>--}}
                    {{--                            <li><a href="tables-datatable.html">Data Table</a></li>--}}
                    {{--                            <li><a href="tables-responsive.html">Responsive Table</a></li>--}}
                    {{--                            <li><a href="tables-editable.html">Editable Table</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-face-smile"></i>--}}
                    {{--                            <span>Icons <span class="float-right menu-arrow"><i--}}
                    {{--                                            class="mdi mdi-chevron-right"></i></span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="icons-material.html">Material Design</a></li>--}}
                    {{--                            <li><a href="icons-fontawesome.html">Font Awesome</a></li>--}}
                    {{--                            <li><a href="icons-ion.html">Ion Icons</a></li>--}}
                    {{--                            <li><a href="icons-themify.html">Themify Icons</a></li>--}}
                    {{--                            <li><a href="icons-dripicons.html">Dripicons</a></li>--}}
                    {{--                            <li><a href="icons-typicons.html">Typicons Icons</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i--}}
                    {{--                                    class="ti-location-pin"></i><span> Maps <span--}}
                    {{--                                        class="badge badge-pill badge-danger float-right">2</span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="maps-google.html">Google Map</a></li>--}}
                    {{--                            <li><a href="maps-vector.html">Vector Map</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="menu-title">Extras</li>--}}
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-archive"></i><span> Authentication <span--}}
                    {{--                                        class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="pages-login.html">Login 1</a></li>--}}
                    {{--                            <li><a href="pages-login-2.html">Login 2</a></li>--}}
                    {{--                            <li><a href="pages-register.html">Register 1</a></li>--}}
                    {{--                            <li><a href="pages-register-2.html">Register 2</a></li>--}}
                    {{--                            <li><a href="pages-recoverpw.html">Recover Password 1</a></li>--}}
                    {{--                            <li><a href="pages-recoverpw-2.html">Recover Password 2</a></li>--}}
                    {{--                            <li><a href="pages-lock-screen.html">Lock Screen 1</a></li>--}}
                    {{--                            <li><a href="pages-lock-screen-2.html">Lock Screen 2</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-support"></i><span> Extra Pages <span--}}
                    {{--                                        class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="pages-timeline.html">Timeline</a></li>--}}
                    {{--                            <li><a href="pages-invoice.html">Invoice</a></li>--}}
                    {{--                            <li><a href="pages-directory.html">Directory</a></li>--}}
                    {{--                            <li><a href="pages-blank.html">Blank Page</a></li>--}}
                    {{--                            <li><a href="pages-404.html">Error 404</a></li>--}}
                    {{--                            <li><a href="pages-500.html">Error 500</a></li>--}}
                    {{--                            <li><a href="pages-pricing.html">Pricing</a></li>--}}
                    {{--                            <li><a href="pages-gallery.html">Gallery</a></li>--}}
                    {{--                            <li><a href="pages-maintenance.html">Maintenance</a></li>--}}
                    {{--                            <li><a href="pages-comingsoon.html">Coming Soon</a></li>--}}
                    {{--                            <li><a href="pages-faq.html">Faq</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    {{--                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-bookmark-alt"></i><span> Email Templates <span--}}
                    {{--                                        class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span></span></a>--}}
                    {{--                        <ul class="submenu">--}}
                    {{--                            <li><a href="email-template-basic.html">Basic Action Email</a></li>--}}
                    {{--                            <li><a href="email-template-Alert.html">Alert Email</a></li>--}}
                    {{--                            <li><a href="email-template-Billing.html">Billing Email</a></li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                </ul>
            </div><!-- Sidebar -->
            <div class="clearfix"></div>
        </div><!-- Sidebar -left --></div><!-- Left Sidebar End -->
    <!-- ============================================================== --><!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page"><!-- Start content -->
        <div class="content">
            <div class="container-fluid">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <h4 class="page-title">Dashboard</h4>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">Welcome to Payment Control Syetem</li>
                            </ol>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-right d-none d-md-block">
                                <!--<div class="dropdown">-->
                                <!--    <button class="btn btn-primary dropdown-toggle arrow-none waves-effect waves-light"-->
                                <!--            type="button" data-toggle="dropdown" aria-haspopup="true"-->
                                <!--            aria-expanded="false"><i class="mdi mdi-settings mr-2"></i> Settings-->
                                <!--    </button>-->
                                <!--    <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Action</a>-->
                                <!--        <a class="dropdown-item" href="#">Another action</a> <a class="dropdown-item"-->
                                <!--                                                                href="#">Something else-->
                                <!--            here</a>-->
                                <!--        <div class="dropdown-divider"></div>-->
                                <!--        <a class="dropdown-item" href="#">Separated link</a></div>-->
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--cards row-->
            @yield('content')
            <!-- end row -->
            </div>
        </div>

        <footer class="footer">© 2019 Payment Control System</footer>
    </div>
    <!-- jQuery  -->
    <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    {{--<script src="{{asset('assets/js/waves.min.js')}}"></script><!--Chartist Chart-->--}}
    {{--<script src="{{asset('plugins/chartist/js/chartist.min.js')}}"></script>--}}
    {{--<script src="{{asset('plugins/chartist/js/chartist-plugin-tooltip.min.js')}}"></script><!-- peity JS -->--}}
    {{--<script src="{{asset('plugins/peity-chart/jquery.peity.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/pages/dashboard.js')}}"></script><!-- App js -->--}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

    <script src="{{asset('assets/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets/js/waves.min.js')}}"></script><!-- Required datatable js -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script><!-- Buttons examples -->
    <script src="{{asset('plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/buttons.colVis.min.js')}}"></script><!-- Responsive examples -->
    <script src="{{asset('plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/responsive.bootstrap4.min.js')}}"></script><!-- Datatable init js -->
    <script src="{{asset('assets/pages/datatables.init.js')}}"></script><!-- App js -->
    <script src="{{asset('assets/js/app.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

@yield('script')
</body>
<!-- Mirrored from themesbrand.com/veltrix/layouts/vertical/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 21 Oct 2019 06:21:11 GMT -->
</html>
