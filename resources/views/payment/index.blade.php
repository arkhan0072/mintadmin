@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-white anime">
                        Payments Overview
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="data" class="table table-bordered" style="width:90%">
                                <thead>
                                <tr class="bg-pale-dark">
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>card_last_4</th>
                                    <th>Function</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Make Payment / Charge Again</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('payment')}}" method="post">
                        @csrf
                        <div class="form-row align-items-center">
                            <input type="hidden" class="form-control" id="customer_id" readonly name="customer_id" ><br>
                            <input type="email" class="form-control" id="email" readonly name="email" ><br>
                            <input type="text" class="form-control" id="card_last_4" readonly name="card_last_4" ><br>
                            <input type="text" class="form-control" id="amount" name="amount" ><br>
                            <input type="submit" name="Charge Again" class="btn btn-primary">

                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).on('click','.btn-primary',function () {

            id = $(this).data('id');
            $.ajax({
                type: "get",
                url: '{{route('makesecondpayment')}}',
                data: {
                    id: id
                },
                success: function (response) {
                    $('#customer_id').val(response.customer_id);
                    $('#email').val(response.name);
                    $('#card_last_4').val(response.card_last_4);
                    $('#amount').val(response.amount);
                    console.log(response);
                }

            });

        });
        var table;
        $(document).ready(function () {
            $('#data thead tr').clone(true).appendTo('#data thead').attr('id', 'tr2');

            $('#data thead tr:eq(1) th').each(function (i) {
                if (i === $('#data thead tr:eq(1) th').length ) {
                    return;
                }
                var title = $(this).text();
                $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');

                $('input', this).on('keyup change', function () {

                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });


            var methodes;
            table = $('#data').DataTable({
                dom: 'lBftip',
                buttons: [


                ],
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,
                responsive: true,
                scrollX: true,
                ajax: {
                    url: '{{route('paymentLogs')}}',
                },
                columns: [
                    {data: 'id', name: 'id', orderable: false},
                    {data: 'name', name: 'Name', orderable: false},
                    {data: 'email', name: 'Email', orderable: false},
                    {data: 'card_last_4', name: 'card_last_4', orderable: false},
                    {data: 'function', name: 'function', orderable: false},
                ],

            });
        });




    </script>

@endsection


