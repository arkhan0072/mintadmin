@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Employee Details</p>
                    </div>

                    <div class="card-body">
                        <div class="">
                            <table id="data1" class="table table-condensed table-striped" style="width:100%">
                                <thead>
                                <tr class="bg-pale-dark">
                                    <th width="100">id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Hourly Wage</th>
                                    <th>Commute Help</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                    $location_id = 1; // change this value from session...
//                                    dd($users);
                                    foreach ($users as $user){
                                        // get payroll data

                                        if(strtolower($user->name) == 'field staff'){

                                            ?>
                                <tr class="bg-pale-dark">
                                    <td>{{ $user->user_id }}</td>
                                    <td><a href="{{url('view_payroll_details/'.$user->user_id)}}">{{ $user->username }}</a></td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->hourly_wage }}</td>
                                    <td>{{ $user->commute_help == 1?'Yes':'No' }}</td>

                                </tr>

                                <?php


                                        }
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {

            $('#data1').DataTable({});
        {{--    var methodes;--}}
        {{--    table = $('#data').DataTable({--}}
        {{--        dom: 'lBtip',--}}
        {{--        buttons: [--}}


        {{--        ],--}}
        {{--        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],--}}
        {{--        processing: true,--}}
        {{--        serverSide: true,--}}
        {{--        responsive: true,--}}
        {{--        scrollX: true,--}}
        {{--        ajax: {--}}
        {{--            url: '{{route('ajaxVistsReport')}}',--}}
        {{--        },--}}
        {{--        columns: [--}}
        {{--            {data: 'sr', Title: 'sr', orderable: false},--}}
        {{--            {data: 'name', Title: 'name', orderable: false},--}}
        {{--            {data: 'email', Title: 'email', orderable: false},--}}
        {{--            {data: 'hourly_wage', Title: 'Hourly Wage', orderable: false},--}}
        {{--            {data: 'billable_hours', Title: 'Billable Hours', orderable: false},--}}
        {{--            {data: 'adjustments', Title: 'Adjustments', orderable: false},--}}
        {{--            {data: 'total', Title: 'Total', orderable: false},--}}
        {{--            {data: 'paid', Title: 'Paid', orderable: false},--}}

        {{--        ],--}}
        {{--        "columnDefs": [--}}

        {{--            {--}}
        {{--                "targets": 0--}}
        {{--            },--}}

        {{--            {--}}
        {{--                "targets": 1--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 2--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 3--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 4--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 5--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 6--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 7--}}
        {{--            }--}}
        {{--        ],--}}
        {{--    });--}}
        });




    </script>
@endsection
