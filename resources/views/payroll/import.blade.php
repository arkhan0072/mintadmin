@extends('master')
@section('content')




    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Import Visits Report (CSV)</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive1">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form action="{{route('saveImportedVisitsReport')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-2">

                                        <label for="importFile" class="">Import Visits: </label>
                                    </div>

                                    <div class="col-md-4">
                                        <input type="file" name="importFile" id="importFile" >

                                    </div>
                                </div><br />

                                <div class="row">
                                    <div class="col-md-2">

                                    </div>

                                    <div class="col-md-4">

                                        <input type="submit" value="Import" class="btn btn-primary">

                                    </div>
                                </div>

                            </form>

                            @if(session()->has('not_exists') && count(session()->get('not_exists')))

                                <span class="text-danger"><b>Warning!</b> Following {{session()->get('not_exists_count')}} names does not exists in system. Skipping that data.</span>
                                <ul>
                                    @foreach(session()->get('not_exists') as $name)

                                        <li>{{$name}}</li>
                                    @endforeach
                                </ul>


                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
