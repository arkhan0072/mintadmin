@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Payroll Details</p>
                    </div>
                    <div class="card-body">


                        <?php
                        $location_id = session()->get('location_id'); // change this value from session...
                        $msg = '';

                        if ($view_data['filter']){
                            $query = "SELECT p.*, (SELECT SUM(duration) FROM payrolls WHERE LCASE(assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 1 AND p_date BETWEEN '".$view_data['from_date']."' AND '".$view_data['to_date']."'   ORDER BY p_date DESC) as work_hours
                                                FROM payrolls p
                                                WHERE LCASE(p.assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 1 AND p_date BETWEEN '".$view_data['from_date']."' AND '".$view_data['to_date']."' ORDER BY p_date DESC

                                            ";

                            $check = DB::select($query);

                            if (count($check) > 0){
                                $msg = "<b>Warning! </b> you selected a date range that was already processed. Showing only from <b>".$check[0]->p_date."</b> forward.";

                                $view_data['from_date'] = date("Y-m-d", strtotime($check[0]->p_date." +1 day"));

                            }
                        }

                        // get payroll data

                        if(strtolower($user->name) == 'field staff'){

                        //TODO: add location id in this query...
                        $query = "SELECT p.*, (SELECT SUM(duration) FROM payrolls WHERE LCASE(assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 0 AND p_date BETWEEN '".$view_data['from_date']."' AND '".$view_data['to_date']."' AND location_id = '".session()->get('location_id')."' ) as work_hours
                                                FROM payrolls p
                                                WHERE LCASE(p.assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 0 AND p_date BETWEEN '".$view_data['from_date']."' AND '".$view_data['to_date']."' AND location_id = '".session()->get('location_id')."'

                                            ";

                        $data = DB::select($query);


                            $commute_help = 0.0;
                            $name = $user->username;
                            $work_hours = 0;
                            $hourly_wage = $user->hourly_wage;

                            $billable_hours = $work_hours + $commute_help;
                            $billable_amount = $hourly_wage * $billable_hours;
                            $adjustments = 0;

                            $adj_query = "SELECT SUM(amount) as adjustments FROM payroll_adjustments WHERE emp_id='".$user->user_id."' AND adjustment_date BETWEEN '".$view_data['from_date']."' AND '".$view_data['to_date']."'";
                            $adj_data = DB::select($adj_query);
                            if ($adj_data[0]->adjustments != null){
                                $adjustments = $adj_data[0]->adjustments;
                            }

                            $total = $billable_amount + $adjustments;

                        if (count($data) > 0){
                            $data = $data[0];

                        //get commute help value.
                        $commute_help = 0.0;
                        if ($user->commute_help == 1){
                            $ch = DB::select("SELECT commute_help, commute_help_hourly_rate FROM location_settings WHERE location_id = '".$location_id."'");

                            $commute_help_hourly_rate = $ch[0]->commute_help_hourly_rate; // this is percentage.
                            $commute_help = $data->work_hours * $commute_help_hourly_rate;
                            /* === old Requirement===
                            $commute_help_percentage = $ch[0]->commute_help; // this is percentage.
                            $commute_help = ($data->work_hours * $commute_help_percentage) / 100;

                            ====*/
                        }

                        $name = $user->username;
                        $work_hours = $data->work_hours;
                        $hourly_wage = $user->hourly_wage;

//                        $billable_hours = $work_hours + $commute_help; // old requirement
                        $billable_hours = $work_hours;
                        $billable_amount = $hourly_wage * $billable_hours;
//                        $billable_amount += $commute_help;
                        $adjustments = 0;

                        $adj_query = "SELECT SUM(amount) as adjustments FROM payroll_adjustments WHERE emp_id='".$user->user_id."' AND adjustment_date BETWEEN '".$view_data['from_date']."' AND '".$view_data['to_date']."'";
                            $adj_data = DB::select($adj_query);
                            if ($adj_data[0]->adjustments != null){
                                $adjustments = $adj_data[0]->adjustments;
                            }
                            $total = $billable_amount + $adjustments + $commute_help;

                        }
                        }

                        ?>



                        <div class="row">
                            <div class="col-md-8">
                                <h5>Select Date Range to process payroll:</h5>

                                <div class="row">

                                    <div class="col-md-3"><label for="from_date">From: </label><input id="from_date" class="form-control" name="from_date" type="date" value="{{$view_data['from_date']}}"></div>

                                    <div class="col-md-3"><label for="to_date">To: </label><input id="to_date" class="form-control" name="to_date" type="date" value="{{$view_data['to_date']}}"></div>

                                    <div class="col-md-2">

                                        <button class="btn btn-primary" style="margin-top: 30px;" onclick="applyFiter()"> Filter</button>
                                    </div>
                                </div>

                                <span class="text-danger">{!! $msg !!}</span>


                            </div>

                            <div class="col-md-4">
                                <table class="table table-condensed table-striped">
                                    <tr>
                                        <th>Name:</th>
                                        <td>{{$user->username}}<br /><small><a href="{{url('view_payroll_details/'.$user->user_id)}}">View Details</a></small></td>
                                    </tr>
                                    <tr>
                                        <th>Hourly Wage:</th>
                                        <td>${{$user->hourly_wage}}</td>
                                    </tr>
                                    <tr>
                                        <th>Adjustments:</th>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#adjustmentsViewModal">${{$adjustments}}</a>
                                            <button class="btn btn-success" data-toggle="modal" data-target="#adjustmentsModal"> + Addjustments</button>

                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Billable Amount:</th>
                                        <td>${{round($billable_amount, 2)}} <small>(Hours: {{$billable_hours}})</small></td>
                                    </tr>
                                    <tr>
                                        <th>Commute Help:</th>
                                        <td>${{round($commute_help, 2)}} </td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td>${{round($total, 2)}} </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <button class="btn btn-info" data-toggle="modal" data-target="#payModal"> Pay Now</button>
                                        </td>
                                        <td>
{{--                                            <button class="btn btn-info"> Pay Now</button>--}}

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br />
                        <div class="">
                            <table id="data1" class="table table-condensed table-striped" style="width:100%">
                                <thead>
                                <tr class="bg-pale-dark">
                                    <th>Date</th>
                                    <th>Times</th>
                                    <th>Completed</th>
                                    <th>Client Name</th>
                                    <th>Assigned To</th>
                                    <th>Job Details</th>
                                    <th>Duration</th>
                                </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $payroll_data = DB::select($query);

                                    foreach ($payroll_data as $p_data){
                                        ?>
                                    <tr>
                                        <td>{{$p_data->p_date}}</td>
                                        <td>{{$p_data->p_times}}</td>
                                        <td>{{$p_data->completed}}</td>
                                        <td>{{$p_data->client_name}}</td>
                                        <td>{{$p_data->assigned_to}}</td>
                                        <td>{{$p_data->job_details}}</td>
                                        <td>{{$p_data->duration}}</td>
                                    </tr>

                                    <?php
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="adjustmentsViewModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Adjustments</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-condensed table-striped">
                        <tr>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Date</th>
                        </tr>

                        <?php

                        $adjustments_data = DB::select("SELECT * FROM payroll_adjustments WHERE adjustment_date BETWEEN '".$view_data['from_date']."' AND '".$view_data['to_date']."' AND emp_id='".$user->user_id."' ");

                        foreach ($adjustments_data as $adjustment) {
                        ?>
                        <tr>
                            <td>${{$adjustment->amount}}</td>
                            <td>{{$adjustment->description}}</td>
                            <td>{{$adjustment->adjustment_date}}</td>
                        </tr>
                        <?php
                        }

                        ?>


                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div id="adjustmentsModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Adjustments</h4>
                </div>
                <div class="modal-body">
{{--                    <form action="{{route('ajax_payroll_adjustments_save')}}" method="post">--}}
                    <form action="{{route('payroll_adjustments_save')}}" method="post">
                        @csrf
                    <table class="table table-condensed table-striped">
                        <tr>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                        <tr>
                            <th><input type="text" class="form-control" name="amount[]"></th>
                            <th><input type="text" class="form-control" name="description[]"></th>
                            <th><input type="date" class="form-control datepicker" value="{{date("Y-m-d")}}" name="adjustment_date[]"></th>
                            <th>
                                <a href="#" class="text-success" onclick="cloneRow(this)" style="font-size: 20px;">+</a> &nbsp;&nbsp;&nbsp;
                                <a href="#" class="text-danger" onclick="removeRow(this)" style="font-size: 25px;">-</a>
                            </th>

                        </tr>
                        <tr>
                            <td colspan="2">
{{--                                <input type="checkbox" id="bank_transaction" name="bank_transaction" value="1">--}}
{{--                                <label for="bank_transaction">Bank Transaction Sent</label>--}}
                            </td>
                            <td colspan="2">
                                <input type="hidden" id="adjFromDate" name="adjFromDate">
                                <input type="hidden" id="adjToDate" name="adjToDate">
                                <input type="hidden" name="emp_id" value="{{$user->user_id}}">
                                <input type="submit" class="btn btn-info" value="Save" name="adjustments_save_btn">
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="payModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Pay - {{$user->username}}</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('ajax_payroll_adjustments_save')}}" id="payForm" method="post">
                        @csrf
                    <table class="table table-condensed table-striped">

                        <tr>
                            <td colspan="2">
{{--                                <input type="checkbox" id="bank_transaction" name="bank_transaction" value="1">--}}
{{--                                <label for="bank_transaction">Bank Transaction Sent</label>--}}
                                <select name="payVia" id="payVia" class="form-control">
                                    <option value="0">--- Select ----</option>
                                    <option value="1">Bank Transaction Sent</option>
                                    @if($user->paypal_email != null || $user->paypal_email != '')
                                        <option value="2">Pay Via PayPal</option>
                                    @endif

                                </select>
                            </td>
                            <td colspan="2">
                                <input type="hidden" id="adjFromDate1" name="adjFromDate">
                                <input type="hidden" id="adjToDate1" name="adjToDate">
                                <input type="hidden" name="emp_id" value="{{$user->user_id}}">
                                <input type="hidden" name="total_amount" value="{{round($total, 2)}}">
                                <input type="submit" class="btn btn-info" value="Pay" name="adjustments_save_btn">
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('script')
    <script>

        function cloneRow(row) {
            var $tr    = $(row).closest('tr');
            var $clone = $tr.clone();
            $clone.find(':text').val('');
            $tr.after($clone);
        }

        function removeRow(row) {
            var $tr    = $(row).closest('tr');
            $tr.remove();
        }
        
        function applyFiter() {
            $.ajax({
                url: '<?php echo route("ajaxApplyPayrollFilter");?>',
                type: 'POST',
                data: {
                    'from_date': $('#from_date').val(),
                    'to_date': $('#to_date').val(),
                },
                headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},
                success: function (data) {
                    window.location.reload();
                },
                error: function (data) {
                    console.log(data.responseText);
                }
            });

            return false;
        }

        var table;

        $(document).ready(function () {
            window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

        $("#from_date").val('{{$view_data['from_date']}}');
        $("#to_date").val( '{{$view_data['to_date']}}');

            $('#data1').DataTable({});

            $('#adjustmentsModal').on('shown.bs.modal', function (e) {
                 $("#adjFromDate").val($("#from_date").val());
                 $("#adjToDate").val($("#to_date").val());
            });

            $('#payModal').on('shown.bs.modal', function (e) {
                 $("#adjFromDate1").val($("#from_date").val());
                 $("#adjToDate1").val($("#to_date").val());
            });

            $('#payVia').on('change', function () {
                var payVia = $(this).val();

                if (payVia == 2){

                    $('#payForm').attr('action', "{{route('payViaPayPal')}} ");

                }
                else if(payVia == 1){
                    $('#payForm').attr('action', "{{route('ajax_payroll_adjustments_save')}} ");
                }
                else{
                    $('#payForm').attr('action', "{{route('ajax_payroll_adjustments_save')}} ");
                }

            });
        {{--    var methodes;--}}
        {{--    table = $('#data').DataTable({--}}
        {{--        dom: 'lBtip',--}}
        {{--        buttons: [--}}


        {{--        ],--}}
        {{--        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],--}}
        {{--        processing: true,--}}
        {{--        serverSide: true,--}}
        {{--        responsive: true,--}}
        {{--        scrollX: true,--}}
        {{--        ajax: {--}}
        {{--            url: '{{route('ajaxVistsReport')}}',--}}
        {{--        },--}}
        {{--        columns: [--}}
        {{--            {data: 'sr', Title: 'sr', orderable: false},--}}
        {{--            {data: 'name', Title: 'name', orderable: false},--}}
        {{--            {data: 'email', Title: 'email', orderable: false},--}}
        {{--            {data: 'hourly_wage', Title: 'Hourly Wage', orderable: false},--}}
        {{--            {data: 'billable_hours', Title: 'Billable Hours', orderable: false},--}}
        {{--            {data: 'adjustments', Title: 'Adjustments', orderable: false},--}}
        {{--            {data: 'total', Title: 'Total', orderable: false},--}}
        {{--            {data: 'paid', Title: 'Paid', orderable: false},--}}

        {{--        ],--}}
        {{--        "columnDefs": [--}}

        {{--            {--}}
        {{--                "targets": 0--}}
        {{--            },--}}

        {{--            {--}}
        {{--                "targets": 1--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 2--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 3--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 4--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 5--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 6--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 7--}}
        {{--            }--}}
        {{--        ],--}}
        {{--    });--}}
        });




    </script>
@endsection
