@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Payroll Details</p>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-2 offset-10">
                                <button class="btn btn-success" data-toggle="modal" data-target="#adjustmentsSaveModal"> + Addjustments</button>
                                <br /> &nbsp;

                            </div>
                            <div class="col-md-12">
                                <select name="payroll_snapshot_select" id="payroll_snapshot_select" class="form-control">
                                    <option value="null" selected>--- Select ---</option>
                                    <?php
                                        foreach ($data['payroll_snapshots'] as $snapshot){
                                            ?>

                                        <option value="{{$data['user']->id}}/{{$snapshot->id}}">{{$snapshot->from_date}} - {{$snapshot->to_date}}</option>
                                    <?php
                                        }
                                    ?>

                                </select>

                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-8">
                                <?php

                                $location_id = 1; // change this value from session...


                                    if (!empty($data['payroll_snapshot'])){
                                        $user = $data['user'];
                                        // get payroll data

                                    //TODO: add location id in this query...
                                    $query = "SELECT p.*, (SELECT SUM(duration) FROM payrolls WHERE LCASE(assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 1 AND p_date BETWEEN '".$data['payroll_snapshot']->from_date."' AND '".$data['payroll_snapshot']->to_date."'  AND location_id = '".session()->get('location_id')."') as work_hours
                                                FROM payrolls p
                                                WHERE LCASE(p.assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 1 AND p_date BETWEEN '".$data['payroll_snapshot']->from_date."' AND '".$data['payroll_snapshot']->to_date."' AND location_id = '".session()->get('location_id')."'

                                            ";

                                    $q_data = DB::select($query);


                                    $commute_help = 0.0;
                                    $name = $user->username;
                                    $work_hours = 0;
                                    $hourly_wage = $user->hourly_wage;

                                    $billable_hours = $work_hours + $commute_help;
                                    $billable_amount = $hourly_wage * $billable_hours;
                                    $adjustments = 0;
                                    $total = $billable_amount + $adjustments;

                                    if (count($q_data) > 0){
                                        $q_data = $q_data[0];

                                        //get commute help value.
                                        $commute_help = 0.0;
                                        if ($user->commute_help == 1){
                                            $ch = DB::select("SELECT commute_help, commute_help_hourly_rate FROM location_settings WHERE location_id = '".$location_id."'");

                                            $commute_help_hourly_rate = $ch[0]->commute_help_hourly_rate; // this is percentage.
                                            $commute_help = $q_data->work_hours * $commute_help_hourly_rate;
                                            /* === old Requirement===
                                            $commute_help_percentage = $ch[0]->commute_help; // this is percentage.
                                            $commute_help = ($data->work_hours * $commute_help_percentage) / 100;

                                            ====*/
                                        }

                                        $name = $user->username;
                                        $work_hours = $q_data->work_hours;
                                        $hourly_wage = $user->hourly_wage;

                                        $billable_hours = $work_hours;
                                        $billable_amount = $hourly_wage * $billable_hours;
//                                        $billable_amount += $commute_help;

//                                        dd($data['payroll_snapshot']);
                                        $adjustments = DB::select("SELECT SUM(amount) as adjustment_amount FROM payroll_adjustments WHERE adjustment_date BETWEEN '".$data['payroll_snapshot']->from_date."' AND '".$data['payroll_snapshot']->to_date."' AND emp_id='".$user->id."' ")[0]->adjustment_amount;
//                                        dd($adjustments);

                                        if ($adjustments == null){
                                            $adjustments = 0;
                                        }
                                        $total = $billable_amount + $adjustments + $commute_help;
                                    }

                                ?>
                                <br />
                                <h5>Payroll details for date range: {{$data['payroll_snapshot']->from_date}} - {{$data['payroll_snapshot']->to_date}}</h5>

                                    <form action="{{route('updatePayrollBankTransactionStatus')}}" method="post">
                                        @csrf
                                        <!--
                                        <input type="checkbox" name="bank_transaction" value="1" <?php echo isset($data['payroll_snapshot']->bank_transaction) && $data['payroll_snapshot']->bank_transaction == 1? 'checked': '';?> id="bank_transaction">
                                        <label for="bank_transaction">Bank Transaction Sent</label> -->

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <select name="payVia" id="" class="form-control">
                                                        <option value="0" <?php echo isset($data['payroll_snapshot']->paid_via) && $data['payroll_snapshot']->paid_via == 0? 'selected': '';?> >--- Select ----</option>
                                                        <option value="1" <?php echo isset($data['payroll_snapshot']->paid_via) && $data['payroll_snapshot']->paid_via == 1? 'selected': '';?> >Bank Transaction Sent</option>
                                                        @if($user->paypal_email != null || $user->paypal_email != '')
                                                            <option value="2" <?php echo isset($data['payroll_snapshot']->paid_via) && $data['payroll_snapshot']->paid_via == 2? 'selected': '';?> >Pay Via PayPal</option>
                                                        @endif

                                                    </select>
                                                </div>

                                                <div class="col-md-1">
                                                    <input type="hidden" name="ps_id" value="{{$data['payroll_snapshot']->id}}">
                                                    <input type="submit" name="submit" value="Update" class="btn btn-primary">
                                                </div>
                                            </div>



                                    </form>


                            </div>

                            <div class="col-md-4">
                                <br />
                                <table class="table table-condensed table-striped">
                                    <tr>
                                        <th>Name:</th>
                                        <td>{{$user->username}}<br /></td>
                                    </tr>
                                    <tr>
                                        <th>Hourly Wage:</th>
                                        <td>${{$user->hourly_wage}}</td>
                                    </tr>
                                    <tr>
                                        <th>Adjustments:</th>
                                        <td><a href="#" data-toggle="modal" data-target="#adjustmentsModal">${{$adjustments}}</a></td>
                                    </tr>
                                    <tr>
                                        <th>Billable Amount:</th>
                                        <td>${{round($billable_amount, 2)}} <small>(Hours: {{$billable_hours}})</small></td>
                                    </tr>
                                    <tr>
                                        <th>Commute Help:</th>
                                        <td>${{round($commute_help, 2)}} </td>
                                    </tr>
                                    <tr>
                                        <th>Total:</th>
                                        <td>${{round($total, 2)}} </td>
                                    </tr>
                                </table>
                            </div>

                            <br />
                            <div class="">
                                <table id="data1" class="table table-condensed table-striped" style="width:100%">
                                    <thead>
                                    <tr class="bg-pale-dark">
                                        <th>Date</th>
                                        <th>Times</th>
                                        <th>Completed</th>
                                        <th>Client Name</th>
                                        <th>Assigned To</th>
                                        <th>Job Details</th>
                                        <th>Duration</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    $payroll_data = DB::select($query);

                                    foreach ($payroll_data as $p_data){
                                    ?>
                                    <tr>
                                        <td>{{$p_data->p_date}}</td>
                                        <td>{{$p_data->p_times}}</td>
                                        <td>{{$p_data->completed}}</td>
                                        <td>{{$p_data->client_name}}</td>
                                        <td>{{$p_data->assigned_to}}</td>
                                        <td>{{$p_data->job_details}}</td>
                                        <td>{{$p_data->duration}}</td>
                                    </tr>

                                    <?php
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="adjustmentsModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Adjustments</h4>
                </div>
                <div class="modal-body">

                        <table class="table table-condensed table-striped">
                            <tr>
                                <th>Amount</th>
                                <th>Description</th>
                                <th>Date</th>
                            </tr>

                            <?php
                            if (!empty($data['payroll_snapshot'])){
                                $adjustments_data = DB::select("SELECT * FROM payroll_adjustments WHERE adjustment_date BETWEEN '".$data['payroll_snapshot']->from_date."' AND '".$data['payroll_snapshot']->to_date."' AND emp_id='".$user->id."' ");

                                foreach ($adjustments_data as $adjustment) {
                                    ?>
                            <tr>
                                <td>${{$adjustment->amount}}</td>
                                <td>{{$adjustment->description}}</td>
                                <td>{{$adjustment->adjustment_date}}</td>
                            </tr>
                            <?php
                                }
                            }
                            ?>


                        </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="adjustmentsSaveModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Adjustments</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('payroll_adjustments_save')}}" method="post">
                        @csrf
                        <table class="table table-condensed table-striped">
                            <tr>
                                <th>Amount</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                            <tr>
                                <th><input type="text" class="form-control" name="amount[]"></th>
                                <th><input type="text" class="form-control" name="description[]"></th>
                                <th><input type="date" class="form-control datepicker" value="{{date("Y-m-d")}}" name="adjustment_date[]"></th>
                                <th>
                                    <a href="#" class="text-success" onclick="cloneRow(this)" style="font-size: 20px;">+</a> &nbsp;&nbsp;&nbsp;
                                    <a href="#" class="text-danger" onclick="removeRow(this)" style="font-size: 25px;">-</a>
                                </th>

                            </tr>
                            <tr>
                                <td colspan="2">

                                </td>
                                <td colspan="2">
                                    <input type="hidden" name="emp_id" value="{{$data['emp_id']}}">
                                    <input type="submit" class="btn btn-info" value="Save Adjustments" name="adjustments_save_btn">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        function cloneRow(row) {
            var $tr    = $(row).closest('tr');
            var $clone = $tr.clone();
            $clone.find(':text').val('');
            $tr.after($clone);
        }

        function removeRow(row) {
            var $tr    = $(row).closest('tr');
            $tr.remove();
        }
        var table;

        $(document).ready(function () {
            window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

            $('#data1').DataTable({});

            $('#payroll_snapshot_select').on('change', function () {
                 var val = $(this).val();
                 if(val != 'null'){
                     window.location.href = "{{url('view_payroll_details')}}"+"/"+val;
                 }
            });
        {{--    var methodes;--}}
        {{--    table = $('#data').DataTable({--}}
        {{--        dom: 'lBtip',--}}
        {{--        buttons: [--}}


        {{--        ],--}}
        {{--        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],--}}
        {{--        processing: true,--}}
        {{--        serverSide: true,--}}
        {{--        responsive: true,--}}
        {{--        scrollX: true,--}}
        {{--        ajax: {--}}
        {{--            url: '{{route('ajaxVistsReport')}}',--}}
        {{--        },--}}
        {{--        columns: [--}}
        {{--            {data: 'sr', Title: 'sr', orderable: false},--}}
        {{--            {data: 'name', Title: 'name', orderable: false},--}}
        {{--            {data: 'email', Title: 'email', orderable: false},--}}
        {{--            {data: 'hourly_wage', Title: 'Hourly Wage', orderable: false},--}}
        {{--            {data: 'billable_hours', Title: 'Billable Hours', orderable: false},--}}
        {{--            {data: 'adjustments', Title: 'Adjustments', orderable: false},--}}
        {{--            {data: 'total', Title: 'Total', orderable: false},--}}
        {{--            {data: 'paid', Title: 'Paid', orderable: false},--}}

        {{--        ],--}}
        {{--        "columnDefs": [--}}

        {{--            {--}}
        {{--                "targets": 0--}}
        {{--            },--}}

        {{--            {--}}
        {{--                "targets": 1--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 2--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 3--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 4--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 5--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 6--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 7--}}
        {{--            }--}}
        {{--        ],--}}
        {{--    });--}}
        });




    </script>
@endsection
