@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Payroll Summary</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="data1" class="table table-condensed table-bordered" style="width:100%">
                                <thead>
                                <tr class="bg-pale-dark">
                                    <th width="20">id</th>
                                    <th>Name</th>
                                    <th>Hourly Wage</th>
                                    <th>Work Hours</th>
                                    <th>Billable Hours</th>
                                    <th>Commute Help</th>
                                    <th>Adjustments</th>
                                    <th>Billable Amount</th>
{{--                                    <th>Adjustments +/-</th>--}}
                                    <th>Total</th>
                                    <th>Paid</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                    $location_id = session()->get('location_id'); // change this value from session...
//                                    dd($users);
                                    foreach ($users as $user){
                                        // get payroll data

                                        if(strtolower($user->name) == 'field staff'){

                                            //TODO: add location id in this query...
                                            $query = "SELECT p.*, (SELECT SUM(duration) FROM payrolls WHERE LCASE(assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 0 AND location_id = '".session()->get('location_id')."' ORDER BY p_date DESC) as work_hours
                                                FROM payrolls p
                                                WHERE LCASE(p.assigned_to) LIKE '%".strtolower($user->username)."%' AND completed='Yes' AND is_paid = 0 AND location_id = '".session()->get('location_id')."' ORDER BY p_date DESC

                                            ";

                                            $data = DB::select($query);

                                            $commute_help = 0.0;
                                            $name = $user->username;
                                            $work_hours = 0;
                                            $hourly_wage = $user->hourly_wage;

                                            $billable_hours = $work_hours + $commute_help;
                                            $billable_amount = $hourly_wage * $billable_hours;
                                            $adjustments = 0;
                                            $total = $billable_amount + $adjustments;

                                            $max_date = date('Y-m-d');
                                            $min_date = date('Y-m-d');
                                            if (count($data) > 0){
                                                $min_date = $data[count($data) - 1]->p_date;
//                                            dd($user);
                                                $data = $data[0];

                                            //get commute help value.
                                            $commute_help = 0.0;
                                            if ($user->commute_help == 1){
                                                $ch = DB::select("SELECT commute_help, commute_help_hourly_rate FROM location_settings WHERE location_id = '".$location_id."'");

                                                $commute_help_hourly_rate = $ch[0]->commute_help_hourly_rate; // this is percentage.
                                                $commute_help = $data->work_hours * $commute_help_hourly_rate;
//                                                $commute_help_percentage = $ch[0]->commute_help; // this is percentage.
//                                                $commute_help = ($data->work_hours * $commute_help_percentage) / 100;
                                            }

                                            $name = $user->username;
                                            $work_hours = $data->work_hours;
                                            $hourly_wage = $user->hourly_wage;

                                            $billable_hours = $work_hours;
//                                            $billable_hours = $work_hours + $commute_help;
                                            $billable_amount = $hourly_wage * $billable_hours;
                                            $adjustments = 0;

                                                $adjustments = DB::select("SELECT SUM(amount) as adjustment_amount FROM payroll_adjustments WHERE adjustment_date BETWEEN '".$min_date."' AND '".$max_date."' AND emp_id='".$user->user_id."' ")[0]->adjustment_amount;
//                                        dd($adjustments);

//                                                dd($min_date);
                                                if ($adjustments == null){
                                                    $adjustments = 0;
                                                }

                                            $total = $billable_amount + $adjustments + $commute_help;
                                            }

                                            ?>
                                <tr class="bg-pale-dark">
                                    <td>{{ $user->user_id }}</td>
                                    <td><a href="{{url('view_payroll/'.$user->user_id)}}">{{ $name }}</a></td>
                                    <td>${{ $hourly_wage }}</td>
                                    <td>{{ $work_hours }}</td>
                                    <td>{{ $billable_hours }}</td>
                                    <td>${{ $commute_help }}</td>
                                    <td>${{ $adjustments }}</td>
                                    <td>${{ round($billable_amount, 2) }}</td>
                                    <td class="text-success">${{ round($total, 2) }}</td>
                                    <td>No</td>
                                </tr>

                                <?php


                                        }
                                    }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {

            $('#data1').DataTable({});
        {{--    var methodes;--}}
        {{--    table = $('#data').DataTable({--}}
        {{--        dom: 'lBtip',--}}
        {{--        buttons: [--}}


        {{--        ],--}}
        {{--        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],--}}
        {{--        processing: true,--}}
        {{--        serverSide: true,--}}
        {{--        responsive: true,--}}
        {{--        scrollX: true,--}}
        {{--        ajax: {--}}
        {{--            url: '{{route('ajaxVistsReport')}}',--}}
        {{--        },--}}
        {{--        columns: [--}}
        {{--            {data: 'sr', Title: 'sr', orderable: false},--}}
        {{--            {data: 'name', Title: 'name', orderable: false},--}}
        {{--            {data: 'email', Title: 'email', orderable: false},--}}
        {{--            {data: 'hourly_wage', Title: 'Hourly Wage', orderable: false},--}}
        {{--            {data: 'billable_hours', Title: 'Billable Hours', orderable: false},--}}
        {{--            {data: 'adjustments', Title: 'Adjustments', orderable: false},--}}
        {{--            {data: 'total', Title: 'Total', orderable: false},--}}
        {{--            {data: 'paid', Title: 'Paid', orderable: false},--}}

        {{--        ],--}}
        {{--        "columnDefs": [--}}

        {{--            {--}}
        {{--                "targets": 0--}}
        {{--            },--}}

        {{--            {--}}
        {{--                "targets": 1--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 2--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 3--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 4--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 5--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 6--}}
        {{--            },--}}
        {{--            {--}}

        {{--                "targets": 7--}}
        {{--            }--}}
        {{--        ],--}}
        {{--    });--}}
        });




    </script>
@endsection
