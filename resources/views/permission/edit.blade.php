@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Users Details</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form action="{{route('permission.update',$permission->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input name="_method" type="hidden" value="PUT">

                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="col-md-6">
                                        <input class="form-control" name="name" type="text"  value="{{ old('name',$permission->name) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Display Name</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="display_name" value="{{ old('display_name',$permission->display_name) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="description" value="{{ old('description',$permission->description) }}">
                                    </div>
                                </div>
                                <input type="submit" name="Create New Role" class="btn btn-lg btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
