<button class="btn btn-danger" onclick="frmdlt{{$role->id}}.submit();"  title="Block"><i class="fa fa-ban"></i></button>
<form name="frmdlt{{$role->id}}" action="{{ route('permission.destroy', $role->id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
