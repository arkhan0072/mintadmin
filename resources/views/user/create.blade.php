@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Users Details</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>User Name</label>
                                    <div class="col-md-6">
                                        <input class="form-control" name="username" type="text"
                                               value="{{ old('username') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="email" name="email"
                                               value="{{ old('email') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Location</label>
                                    <div class="col-md-6">
                                        <select name="location" id="" class="form-control">
                                            @foreach($locations as $location)
                                                <option value="{{$location->id}}">{{$location->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="password" name="password"
                                               value="{{ old('password') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Assign Role</label>
                                    <div class="col-md-6">
                                        <select name="role" class="form-control" id="roles">
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="hide">
                                    <label>Hourly Wage</label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="hourly_wage"
                                               value="{{ old('hourly_wage') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Assign Permission</label>
                                    <div class="col-md-6">
                                        <select name="permission[]" class="form-control" multiple>
                                            @foreach($locations as $location)
                                            @foreach($location->policies as $policy)
                                                <option value="{{$policy->pivot->id}}">{{$policy->name}}-({{$location->name}})</option>
                                            @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label>Assign Models</label>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <select name="models[]" class="form-control" multiple>--}}
{{--                                            @foreach($models as $model)--}}
{{--                                                <option value="{{$model}}">{{$model}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <input type="submit" name="Create New User" class="btn btn-lg btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@section('script')
    <script>
        $(document).ready(function () {


            $('#hide').hide();
            $('#roles').on('change', function () {
                var role = $(this).children("option:selected").text();
                if (role == 'office staff' || role == 'field staff') {
                    $('#hide').show();
                } else {
                    $('#hide').hide();
                }

            })
        })
    </script>
@endsection
@stop
