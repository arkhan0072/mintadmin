<button class="btn btn-danger" onclick="frmdlt{{$data->user_id}}.submit();"  title="Block"><i class="fa fa-ban"></i></button>
<form name="frmdlt{{$data->user_id}}" action="{{ route('user.destroy', $data->user_id)}}" method="post">
    {!! method_field('delete') !!}
    {{csrf_field()}}
</form>
