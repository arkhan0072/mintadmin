@extends('master')
@section('content')
    <div class="card">

        <div class="row">
            <div class="col-md-12">
                <div class="card bg-gray-light ">
                    <div class="card-header text-black anime">
                        <p class="text-black-50">Users Details</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="data"  class="table-striped"
                                   style="width: 100%;">
                                <thead>
                                <tr class="bg-pale-dark">
                                    <th>Id</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Roles</th>
                                    <th>Location Name</th>
                                    <th>Functions</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        var table;
        $(document).ready(function () {


            var methodes;
            table = $('#data').DataTable({
                dom: 'lBtip',
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,
                responsive: false,
                scrollX: true,
                bStateSave: true,
                ajax: {
                    url: '{{route('ajaxUsers')}}',
                },
                columns: [
                    {data: 'user_id', Title: 'id', orderable: false},
                    {data: 'username', Title: 'name', orderable: false},
                    {data: 'email', Title: 'email', orderable: false},
                    {data: 'display_name', Title: 'Role', orderable: false},
                    {data: 'Location Name', Title: 'Location Name', orderable: false},
                    {data: 'function', Title: 'Function', orderable: false},
                ],
                "columnDefs": [

                    {
                        "targets": 0
                    },

                    {
                        "targets": 1
                    },
                    {

                        "targets": 2
                    },
                    {

                        "targets": 3
                    },
                    {

                        "targets": 4
                    }
                ],
            });
        });




    </script>
@endsection
