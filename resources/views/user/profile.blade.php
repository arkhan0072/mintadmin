@extends('master')
@section('content')


    <!-- begin register -->
    <div class="register register-with-news-feed">
        <!-- begin news-feed -->

        <div class="register-content">

            <form action="{{route('updateProfile',$user->id)}}" method="post" class="margin-bottom-0">
                {{csrf_field()}}
                <label class="control-label">User Name <span class="text-danger">*</span></label>
                <div class="row row-space-10">
                    <div class="col-md-12 m-b-15">
                        <input type="text" class="form-control {{ $errors->has('name') ? 'has-error': ''}}" name="name" placeholder="User name"  value="{{old('user_name',$user->name)}}" />
                        @if($errors->has('name'))
                            <span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
                        @endif
                    </div>
                </div>

                <label class="control-label">Email <span class="text-danger">*</span></label>
                <div class="row m-b-15">
                    <div class="col-md-12">
                        <input type="email" class="form-control {{ $errors->has('email') ? 'has-error': ''}}" name="email"  placeholder="Primary Email address"  value="{{old('email',$user->email)}}" />
                        @if($errors->has('email'))
                            <span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
                        @endif
                    </div>
                </div>
                <label class="control-label">Password <span class="text-danger">*</span></label>
                <div class="row m-b-15">
                    <div class="col-md-12">
                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'has-error': ''}}" placeholder="Password"  />
                        <P>Only Enter password When you need to updated it</P>
                        @if($errors->has('password'))
                            <span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
                        @endif
                    </div>
                </div>
{{--                <label class="control-label">Secondary Email</label>--}}
{{--                <div class="row m-b-15">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <input type="email" class="form-control {{ $errors->has('sec_email') ? 'has-error': ''}}" name="sec_email" placeholder="Secondary Email address"  value="{{old('sec_email',$user->sec_email)}}" />--}}
{{--                        @if($errors->has('sec_email'))--}}
{{--                            <span class="help-block">--}}
{{--					<strong>{{ $errors->first('sec_email') }}</strong>--}}
{{--				</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <label class="control-label">Phone No. <span class="text-danger">*</span></label>--}}
{{--                <div class="row m-b-15">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <input type="text" class="form-control {{ $errors->has('phone') ? 'has-error': ''}}" name="phone" placeholder="Enter Your Phone No."  value="{{old('phone',$user->phone_no)}}" />--}}
{{--                        @if($errors->has('phone'))--}}
{{--                            <span class="help-block">--}}
{{--					<strong>{{ $errors->first('phone') }}</strong>--}}
{{--				</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <label class="control-label">Address</label>--}}
{{--                <div class="row m-b-15">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <input type="text" class="form-control {{ $errors->has('address') ? 'has-error': ''}}" name="address" placeholder="Enter Your Address"  value="{{old('address',$user->address)}}" />--}}
{{--                        @if($errors->has('address'))--}}
{{--                            <span class="help-block">--}}
{{--					<strong>{{ $errors->first('address') }}</strong>--}}
{{--				</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <label class="control-label">Website</label>--}}
{{--                <div class="row m-b-15">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <input type="text" class="form-control {{ $errors->has('website') ? 'has-error': ''}}" name="website" placeholder="Enter Your Website."  value="{{old('website',$user->website)}}" />--}}
{{--                        @if($errors->has('website'))--}}
{{--                            <span class="help-block">--}}
{{--					<strong>{{ $errors->first('website') }}</strong>--}}
{{--				</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <label class="control-label">LinkedIn</label>--}}
{{--                <div class="row m-b-15">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <input type="text" class="form-control {{ $errors->has('linkedin') ? 'has-error': ''}}" name="linkedin" placeholder="Enter Your linkedin."  value="{{old('linkedin',$user->linkedin)}}" />--}}
{{--                        @if($errors->has('linkedin'))--}}
{{--                            <span class="help-block">--}}
{{--					<strong>{{ $errors->first('linkedin') }}</strong>--}}
{{--				</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}

                {{--<div class="checkbox checkbox-css m-b-30">--}}
                {{--<div class="checkbox checkbox-css m-b-30">--}}
                {{--<input type="checkbox" id="agreement_checkbox" value="">--}}
                {{--<label for="agreement_checkbox">--}}
                {{--By clicking Sign Up, you agree to our <a href="javascript:;">Terms</a> and that you have read our <a href="javascript:;">Data Policy</a>, including our <a href="javascript:;">Cookie Use</a>.--}}
                {{--</label>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="register-buttons">
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Up Date</button>
                </div>
                {{--<div class="m-t-20 m-b-40 p-b-40">--}}
                {{--Already a member? Click <a href="login_v3.html">here</a> to login.--}}
                {{--</div>--}}
                <hr />

            </form>
        </div>
    </div>
@stop
