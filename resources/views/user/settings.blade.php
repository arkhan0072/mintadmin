@extends('master')
@section('content')
    <!-- new form-->

    <div class="col-md-6 offset-3">
        <div class="x_panel">
            <div class="x_title">
                <h2>Settings</h2>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-info"></i> Success!</h4>
                        {{Session::get('message')}}
                    </div>
                @endif
                <div class="x_content">
                    <br>
                    <form method="post" action="{{route('settings.update')}}" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                        {{csrf_field()}}


                        <div class="form-group {{ $errors->has('stripe_pk') ? 'has-error': ''}} ">
                            <label>stripe_pk</label>
                            <input type="text" class="form-control" name="stripe_pk"  value="{{old('stripe_pk',setting('stripe_pk'))}}" >
                            @if($errors->has('stripe_pk'))
                                <span class="help-block">
					<strong>{{ $errors->first('stripe_pk') }}</strong>
				</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('stripe_sk') ? 'has-error': ''}} ">
                            <label>stripe_sk</label>
                            <input type="text" class="form-control" name="stripe_sk"  value="{{old('stripe_sk',setting('stripe_sk'))}}" >
                            @if($errors->has('stripe_sk'))
                                <span class="help-block">
					<strong>{{ $errors->first('stripe_sk') }}</strong>
				</span>
                            @endif
                        </div>


                        <br>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                    <!--ads-->
                </div>
            </div>
        </div>
    </div>
@section('scripts')

@endsection
@stop
