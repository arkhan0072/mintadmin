<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(["middleware" => 'auth'], function () {
//orland system
    Route::get('/dashboard', 'OrlandoController@dashboard')->name('dashboard');
    Route::get('/', 'OrlandoController@index')->name('home');
    Route::post('/payment', 'PaymentController@payment')->name('payment');
    Route::post('/import', 'OrlandoController@import')->name('import');
    Route::get('paymentdata', 'OrlandoController@paymentdata')->name('paymentdata');
    Route::get('/logs', 'LogController@index')->name('logs');
    Route::get('/getlogs', 'LogController@getlogs')->name('getlogs');
    Route::get('/orland', 'OrlandoController@edit')->name('editOrlando');
    Route::post('/updateOrlando', 'OrlandoController@update')->name('updateOrlando');
 //system Payment Logs
    Route::get('/payment','PaymentController@index')->name('paymentindex');
    Route::get('/paymentLogs','PaymentController@paymentLogs')->name('paymentLogs');
    Route::get('/makesecondpayment','PaymentController@makesecondpayment')->name('makesecondpayment');
//user system
    Route::resource('user', 'UserController');
    Route::resource('location', 'LocationController');
//    Route::get('/delete/{location}', 'LocationController@delete')->name('delete');
    Route::get('ajaxdata', 'LocationController@ajaxdata')->name('ajaxdata');
    Route::resource('form', 'FormController');
    Route::get('ajaxForms', 'FormController@ajaxForms')->name('ajaxForms');
    Route::get('ajaxUsers', 'UserController@ajaxUsers')->name('ajaxUsers');
    Route::get('userprofile', 'UserController@showProfile')->name('profileview');
    Route::post('profile', 'UserController@updateProfile')->name('updateProfile');
//settings for payment gateways
    Route::get('/settings', 'SettingController@show')->name('settings.show');
    Route::post('/settings', 'SettingController@update')->name('settings.update');
//Roles system
    Route::resource('role', 'RoleController');
    Route::get('ajaxRole', 'RoleController@ajaxRole')->name('ajaxRole');
//permission system
    Route::resource('permission', 'PermissionController');
    Route::get('/ajaxPermission', 'PermissionController@ajaxPermission')->name('ajaxPermission');


    // payroll routes.. Added By Asad..
    Route::get('/payroll_import', 'payrollController@payroll_import')->name('payroll_import');
    Route::get('/visits_report', 'payrollController@visits_report')->name('visits_report');
    Route::post('/saveImportedVisitsReport', 'payrollController@import')->name('saveImportedVisitsReport');
    Route::get('ajaxVistsReport', 'payrollController@ajaxVistsReport')->name('ajaxVistsReport');
    Route::post('ajax_payroll_adjustments_save', 'payrollController@ajax_payroll_adjustments_save')->name('ajax_payroll_adjustments_save');
    Route::post('payViaPayPal', 'payrollController@payViaPayPal')->name('payViaPayPal');
    Route::post('payroll_adjustments_save', 'payrollController@payroll_adjustments_save')->name('payroll_adjustments_save');
    Route::post('ajaxApplyPayrollFilter', 'payrollController@ajaxApplyPayrollFilter')->name('ajaxApplyPayrollFilter');
    Route::get('/view_payroll/{id}', 'payrollController@view_payroll');
    Route::get('/employee_detail', 'payrollController@employee_detail')->name('employee_detail');
    Route::get('/view_payroll_details/{id}/{payroll_id?}', 'payrollController@view_payroll_details');
    Route::get('/paypal/complete_transaction/{fd}/{td}/{ei}/{pv}', 'payrollController@complete_transaction');
    Route::post('updatePayrollBankTransactionStatus', 'payrollController@updatePayrollBankTransactionStatus')->name('updatePayrollBankTransactionStatus');
});
Route::get('/payment/{token}','FormController@showAmount')->name('publicForm');
Auth::routes();



